'use strict';

angular.module('salesApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'restangular'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, RestangularProvider) {
    $urlRouterProvider
      .otherwise('/products');

    RestangularProvider.addResponseInterceptor(function (data, op) {
      if (op === 'getList' && !angular.isArray(data)) {
        return data.content;
      }
      return data;
    });
    RestangularProvider.setBaseUrl('/api');
    //$locationProvider.html5Mode(true);
  });
