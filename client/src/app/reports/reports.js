(function () {
  'use strict';

  angular.module('salesApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('main.reports', {
          url: '/reports',
          templateUrl: 'app/reports/reports.html',
          controller: 'ReportCtrl'
        });
    })
    .factory('Report', function (Restangular) {
      var ReportService = {
      };

      var AdvancedReport = Restangular.all('report/advanced'),
        DailyReport = Restangular.all('report/daily/' + moment(new Date()).format('YYYY-MM-DD'));

      function fetchReports() {
        ReportService.dailyReports = DailyReport.getList().$object;
        ReportService.advancedReports = AdvancedReport.getList().$object;
      }
      fetchReports();
      return ReportService;
    });

})();
