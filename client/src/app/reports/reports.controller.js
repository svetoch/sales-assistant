(function () {
  'use strict';

  angular.module('salesApp').controller('ReportCtrl', function ($scope, Report, Sale) {
    $scope.advancedReports = Report.advancedReports;
    $scope.sales = Sale.sales;
  });
})();
