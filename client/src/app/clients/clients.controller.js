'use strict';

angular.module('salesApp').controller('ClientsCtrl', function ($scope, $modal, Client) {
  $scope.searchOptions = [{name: 'name', label: 'ФИО'}];
  $scope.searchText = '';
  $scope.activeOption = $scope.searchOptions[0];
  $scope.clients = Client.getList().$object;
  $scope.edit = function (client) {
    var modal = $modal.open({
      templateUrl: 'app/clients/client-edit.html',
      resolve: {
        client: function () {
          return client;
        }
      },
      controller: function ($scope, client) {
        $scope.client = client;
      }
    });
    modal.result.then(function (client) {
      client.put();
      $scope.clients = Client.getList().$object;
    })
  };
  $scope.remove = function (client) {
    client.remove();
    $scope.clients = Client.getList().$object;
  };
});
