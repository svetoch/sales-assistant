'use strict';

angular.module('salesApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.clients', {
        url: '/clients',
        templateUrl: 'app/clients/clients.html',
        controller: 'ClientsCtrl'
      });
  })
  .factory('Client', function (Restangular) {
    return Restangular.all('clients');
  });
