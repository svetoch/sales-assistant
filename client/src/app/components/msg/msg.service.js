angular.module('salesApp').factory('Msg', function ($modal) {
  return function (msg, title) {
    return $modal.open({
      templateUrl: 'app/components/msg/msg.html',
      controller: function ($scope) {
        $scope.msg = msg;
        $scope.title = title;
      }
    });
  }
});
