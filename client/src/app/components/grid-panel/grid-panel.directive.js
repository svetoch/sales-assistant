'use strict';

angular.module('salesApp').directive('gridPanel', function () {
  return {
    restrict: 'E',
    transclude: true,
    link: function (scope) {
      scope.setActive = function (option) {
        scope.activeOption = option;
      }
    },
    templateUrl: 'app/components/grid-panel/grid-panel.html'
  };
});
