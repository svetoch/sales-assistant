angular.module('salesApp').directive('navbar', function () {
  'use strict';
  return {
    restrict: 'E',
    templateUrl: 'app/components/navbar/navbar.html'
  };
});
