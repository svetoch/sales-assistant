(function () {
  'use strict';

  angular.module('salesApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('main.products', {
          url: '/products',
          templateUrl: 'app/products/products.html',
          controller: 'ProductCtrl'
        });
    })
    .factory('Product', function (Restangular, $modal, Category) {
      var RestProducts = Restangular.all('products');
      RestProducts.addRestangularMethod('lowAmount', 'getList', undefined, {filter: 'criticalAmount'});

      var params = {filter: []};

      function fetchProducts() {
        ProductService.products = RestProducts.getList(params).$object;
        ProductService.lowProducts = RestProducts.lowAmount().$object;
      }

      var ProductService = {
        filterByCategory: function (category) {
          ProductService.filter('category', category ? category.id : null);
        },
        filter: function (name, val) {
          params.filter.forEach(function (e, i, a) {
            if (e.indexOf(name) == 0) {
              a.splice(i, 1);
            }
          });

          if (val) {
            params.filter.push(name + ':' + val);
          }

          fetchProducts();
        },
        getListByName: function (name) {
          return RestProducts.getList({filter: ['name:' + name]});
        },
        get: function (id) {
          return RestProducts.one(id).get();
        },
        remove: function (product) {
          return product.remove().then(fetchProducts);
        },
        save: function (product, add) {
          product.category = _.pick(product.category, 'id');
          return add ? RestProducts.post(product) : product.put();
        },
        showEdit: function (product, add) {
          add = (add === undefined) ? !product : add;
          product = product || ProductService.create();

          var modal = $modal.open({
            templateUrl: 'app/products/edit/product-edit.html',
            controller: 'EditProductCtrl',
            resolve: {
              add: function () {
                return add;
              },
              product: function () {
                return product;
              }
            }
          });
          var result = modal.result;
          result.then(fetchProducts);
          return result;
        },
        create: function () {
          return {};
        }
      };

      fetchProducts();

      return ProductService;
    });
})();



