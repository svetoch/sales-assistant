'use strict';

angular.module('salesApp')
  .controller('ProductCtrl', function ($scope, $modal, Product, Category, Invoice) {
    function fetchProducts() {
      $scope.products = Product.products;
      $scope.lowProducts = Product.lowProducts;
    }
    fetchProducts();

    $scope.searchOptions = [{name: 'name', label: 'Наименование'}];
    $scope.activeOption = $scope.searchOptions[0];
    $scope.searchText = '';
    $scope.$watch('searchText', function (val) {
      Product.filter($scope.activeOption.name, val);
      fetchProducts();
    });

    $scope.showAddInvoice = Invoice.showAdd;
    $scope.remove = function (product) {
      Product.remove(product).then(fetchProducts);
    };
    $scope.edit = function (product) {
      Product.showEdit(product).then(fetchProducts);
    };
    $scope.add = function () {
      Product.showEdit().then(fetchProducts);
    };
    $scope.showLow = false;
    $scope.$watch('showLow', function (val) {
      if (val) {
        $scope.products = Product.lowProducts;
      } else {
        $scope.products = Product.products;
      }
    });

    function fetchCategories() {
      $scope.categories = Category.categories;
      $scope.currentCat = Category.current;
    }
    fetchCategories();

    $scope.setCategory = function (category) {
      Category.current = category;
      $scope.currentCat = category;
      Product.filterByCategory(category);
      fetchProducts();
    };
    $scope.editCategory = function (category) {
      Category.showEdit(category).then(fetchCategories);
    };
  });
