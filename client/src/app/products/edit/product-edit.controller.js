angular.module('salesApp').controller('EditProductCtrl', function ($scope, $modalInstance, Product, product, Category, add) {
  $scope.product = product;
  $scope.add = add;
  $scope.categories = Category.categories;
  $scope.save = function () {
    Product.save($scope.product, add).then(function (product) {
      $modalInstance.close(product);
    });
  };
});
