angular.module('salesApp').controller('EditCategoryCtrl', function ($scope, Category, category, add) {
  $scope.category = category;
  $scope.save = function (category) {
    Category.save(category, add).then(function () {
      $scope.$close();
    })
  };
});
