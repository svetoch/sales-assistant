'use strict';

angular.module('salesApp').factory('Category', function ($modal, Restangular) {
  var Category = Restangular.all('products/categories');

  function fetchCategories() {
    CategoryService.categories = Category.getList().$object;
  }

  var CategoryService = {
    current: null,
    showEdit: function (category) {

      var add = !category;
      category = category || CategoryService.create();

      var modal = $modal.open({
        templateUrl: 'app/products/categories/category-edit.html',
        controller: 'EditCategoryCtrl',
        resolve: {
          add: function () {
            return add;
          },
          category: function () {
            return category;
          }
        }
      });
      return modal.result.then(fetchCategories);
    },
    save: function (category, add) {
      return add ? Category.post(category) : category.put();
    },
    create: function () {
      return {name: ''};
    }
  };

  fetchCategories();

  return CategoryService;
});
