'use strict';

angular.module('salesApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.invoices', {
        url: '/products/invoices',
        templateUrl: 'app/products/invoices/invoices.html',
        controller: 'InvoicesCtrl'
      })
      .state('main.add-invoice', {
        url: '/products/invoices/add',
        templateUrl: 'app/products/invoices/add/add-invoice.html',
        controller: 'AddInvoiceCtrl'
      });
  })
  .factory('Invoice', function (Restangular, $modal, Product, $q) {
    var Invoices = Restangular.all('invoices');

    function fetchInvoices() {
      var promise = Invoices.getList();
      InvoiceService.invoices = promise.$object;
      promise.then(function (invoices) {
        invoices.forEach(function (invoice) {
          invoice.getList('items').then(function (items) {
            invoice.items = items;
          });
        })
      });
    }

    var InvoiceService = {
      findByProductName: function (name, item) {
        var $p = Product.getListByName(name);
        return $p.then(function (products) {
          if (!products.length) {
            return $q.reject();
          }

          if (products.length === 1) {
            item.product = products[0];
          }
          return _.map(products, function (p) {
            return p.name;
          });
        });
      },
      save: function (invoice) {
        invoice.date = moment(new Date(invoice.date)).format('DD.MM.YYYY');
        invoice.items = _.map(invoice.items, function (item) {
          item.product = _.pick(item.product, 'id');
          return _.pick(item, 'product', 'amount', 'price');
        });
        return Invoices.post(invoice).then(fetchInvoices);
      },
      remove: function (invoice) {
        return invoice.remove().then(fetchInvoices);
      },
      createItem: function () {
        return {
          product: {id: '', name: '', price: 0},
          price: 0,
          amount: 0,
          incomePrice: 0
        };
      },
      create: function () {
        return {
          code: '',
          date: Date.now(),
          items: []
        }
      },
      addItem: function (invoice, item) {
        invoice.items.push(item);
      },
      showAdd: function () {
        $modal.open({
          templateUrl: 'app/products/invoices/add/add-invoice.html',
          controller: 'AddInvoiceCtrl'
        });
      }
    };

    fetchInvoices();

    return InvoiceService;
  });
