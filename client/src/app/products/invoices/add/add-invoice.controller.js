'use strict';

angular.module('salesApp').controller('AddInvoiceCtrl', function ($scope, $state, Invoice, Product) {
  function create() {
    $scope.invoice = Invoice.create();
    $scope.newItem = Invoice.createItem();
  }
  create();

  $scope.addItem = function (invoice, item) {
    Invoice.addItem(invoice, item);
    $scope.newItem = Invoice.createItem();
  };

  $scope.notFound = false;
  $scope.productNames = [];
  function findByName(val) {
    if (val.length > 2) {
      Invoice.findByProductName(val, $scope.newItem).then(function (productNames) {
        $scope.notFound = false;
        $scope.productNames = productNames;
      }).catch(function () {
          $scope.notFound = true;
      });
    } else {
      $scope.notFound = false;
    }
  }
  $scope.$watch('newItem.product.name', findByName);

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = !$scope.opened;
  };

  $scope.newProduct = function (id) {
    Product.showEdit({id: id}, true).then(function (product) {
      findByName(product.id);
    });
  };

  $scope.save = function (invoice) {
    if ($scope.newItem.product.id && !$scope.notFound) {
      Invoice.addItem(invoice, $scope.newItem);
    }
    Invoice.save(invoice).then(function () {
      $state.go('main.invoices');
    });
  };
});
