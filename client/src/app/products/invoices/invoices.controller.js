(function() {
    'use strict';
    angular.module('salesApp').controller('InvoicesCtrl', function($scope,
        Invoice) {
        $scope.invoices = Invoice.invoices;
        $scope.remove = function(invoice) {
            Invoice.remove(invoice).then(function() {
                $scope.invoices = Invoice.invoices;
            });
        }
    });
})();
