(function () {
  'use strict';

  angular.module('salesApp').directive('discountType', function () {
    var Icons = {
      PRODUCT: 'glyphicon-tasks',
      CATEGORY: ''
    },
      defaultIcon = 'glyphicon-pencil';

    return {
      restrict: 'AE',
      scope: {
        discountType: '=type'
      },
      link: function (scope) {
        scope.iconClass = Icons[scope.discountType] || defaultIcon;
      },
      template: '<span class="glyphicon" ng-class="iconClass"></span>'
    }
  })
})();
