'use strict';

angular.module('salesApp').controller('OrderCtrl', function ($scope, Msg, Product, Client, Sale) {
  function emptyText() {
    $scope.notFound = null;
    $scope.searchText = '';
  }

  emptyText();


  $scope.searchOptions = [{name: 'id', label: 'Штрих-код'}];
  $scope.activeOption = $scope.searchOptions[0];
  $scope.sale = Sale.create();
  $scope.discountTypes = Sale.discountType;

  $scope.save = function () {
    $scope.sale.save().then(function () {
      $scope.sale = Sale.create();
      Msg('Покупка успешно совершена');
    }).catch(function () {
      Msg('Не удалось совершить покупку');
    });
  };

  $scope.$watch('searchText', function (val) {
    if (val.length < 10) {
      if (!val.length) $scope.notFound = false;
      return;
    }

    if (val.indexOf('CLI') !== -1) {
      var $promise = Client.one(val).get();
      $promise.then(emptyText);
      $scope.sale.client = $promise.$object;
    } else {
      $scope.sale.addItem(val).then(emptyText).catch(function () {
        $scope.notFound = val;
      });
    }
  });
}).controller('OrderItemCtrl', function ($scope) {
  $scope.$watch('item.discount', $scope.sale.updateTotals);
  $scope.$watch('item.amount', $scope.sale.updateTotals);
  $scope.$watch('item.isBulk', $scope.sale.updateTotals);
});
