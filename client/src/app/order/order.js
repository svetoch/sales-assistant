'use strict';

angular.module('salesApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.order', {
        url: '/order',
        templateUrl: 'app/order/order.html',
        controller: 'OrderCtrl'
      });
  })
  .factory('Sale', function (Restangular, $q, Product) {
    var DiscountType = {NONE: 'NONE', PRODUCT: 'PRODUCT', CUSTOM: 'CUSTOM', BULK: 'BULK'};

    Restangular.extendModel('sales', function (model) {
      model.items = [];
      model.save = function () {
        var m = _.clone(model);
        m.items = _.map(m.items, function (i) {
          return _.omit(i, 'product');
        });
        m.client = _.pick(m.client, 'id');

        return m.post();
      };
      model.addItem = function (itemId) {
        var item = _.find(model.items, function (i) {
          return i.original.id === itemId;
        });

        var promise;
        if (item) {
          item.amount++;
          var d = $q.defer();
          d.resolve();
          promise = d.promise;
        } else {
          promise = Product.get(itemId).then(function (product) {
            model.items.push({
              discountType: DiscountType.NONE,
              discount: product.discount.discount,
              original: {id: product.id},
              product: product,
              amount: 1,
              name: product.name
            });
          });
        }

        promise.then(model.updateTotals);
        return promise;
      };
      model.removeItem = function (item) {
        var idx = model.items.indexOf(item);
        model.items.splice(idx, 1);
        model.updateTotals();
      };
      model.updateTotals = function () {
        model.items.forEach(function (i) {
          if (i.isBulk) {
            i.discountType = DiscountType.BULK;
            i.price = i.product.discount.bulkPrice;
            i.total = i.amount * i.price;
          } else {
            if (i.discount == i.product.discount.discount) {
              i.discountType = DiscountType.PRODUCT;
            } else {
              i.discountType = DiscountType.CUSTOM;
            }

            i.price = i.product.price;
            i.total = i.amount * i.price * (100 - i.discount) * 0.01
          }
        });
        model.subTotal = model.items.reduce(function (m, i) {
          return m + i.amount * i.product.price;
        }, 0);
        model.total = model.items.reduce(function (m, i) {
          return m + i.amount * i.product.price * (100 - i.discount) * 0.01;
        }, 0);
      };
      return model;
    });

    function fetchSales() {
      var promise = Restangular.all('sales').getList();
      SaleService.sales = promise.$object;
      promise.then(function (sales) {
        sales.forEach(function (sale) {
          sale.getList('items').then(function (items) {
            sale.items = items;
          });
        });
      });
    }

    var SaleService = {
      DiscountType: DiscountType,

      create: function () {
        var sale = Restangular.one('sales');
        sale.paymentMethod = 'CASH';
        return sale;
      }
    };
    fetchSales();

    return SaleService;
  });
