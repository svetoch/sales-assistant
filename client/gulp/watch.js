'use strict';

var gulp = require('gulp');

gulp.task('watch', ['wiredep', 'wiredev'], function () {
  gulp.watch(['src/app/**/*.less'], ['styles']);
  gulp.watch(['src/app/**/*.html'], ['partials']);
  gulp.watch(['src/app/**/*.js'], ['scripts', 'wiredev']);
  gulp.watch('src/images/**/*', ['images']);
  gulp.watch('bower.json', ['wiredep']);
});
