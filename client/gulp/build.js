'use strict';

var DIST = '../src/main/resources/static',
  TEMPLATES = '../src/main/resources/templates';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license']
});

function styles(src, dest) {
  return gulp.src(src)
    .pipe($.plumber())
    .pipe($.less())
    .pipe($.autoprefixer('last 1 version'))
    .pipe($.concat(dest))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe($.size());
}

gulp.task('styles-vendor', function () {
  return styles('src/assets/styles/vendor.less', 'vendor.css');
});

gulp.task('styles', ['styles-vendor'], function () {
  return styles('src/app/**/*.less', 'main.css');
});

gulp.task('scripts', function () {
  return gulp.src('src/app/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.size());
});

gulp.task('partials', function () {
  return gulp.src('src/app/**/*.html')
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.ngHtml2js({
      moduleName: 'salesApp',
      prefix: 'app/'
    }))
    .pipe($.concat('partials.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest('.tmp/partials'))
    .pipe($.size());
});

gulp.task('wiredev', ['scripts'], function () {
  return gulp.src('src/*.html').pipe($.inject(
    gulp.src(['src/app/**/*.js', '!src/app/**/*.spec.js']).pipe($.angularFilesort()), {
      starttag: '<!-- inject:app -->',
      addRootSlash: false,
      relative: true
    }
  )).pipe(gulp.dest('src'))
});

gulp.task('html', ['styles', 'scripts', 'partials', 'wiredev'], function () {
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var htmlFilter = $.filter('**/*.html');
  var nonHtmlFilter = $.filter('!**/*.html');

  return gulp.src('src/*.html')
    .pipe($.inject(gulp.src('.tmp/partials/**/*.js'), {
      read: false,
      starttag: '<!-- inject:partials -->',
      addRootSlash: false,
      addPrefix: '../'
    }))
    .pipe($.useref.assets())
    /*.pipe($.rev())*/
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    /*.pipe($.uglify({preserveComments: $.uglifySaveLicense}))*/
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe($.useref.restore())
    .pipe($.useref())
    /*.pipe($.revReplace())*/
    .pipe(nonHtmlFilter)
    .pipe(gulp.dest(DIST))
    .pipe(nonHtmlFilter.restore())
    .pipe($.size())
    .pipe(htmlFilter)
    .pipe($.htmltidy())
    .pipe(gulp.dest(TEMPLATES));
});

gulp.task('images', function () {
  return gulp.src('src/assets/images/**/*')
    .pipe($.cache($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(DIST + '/images'))
    .pipe($.size());
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest(DIST + '/fonts'))
    .pipe($.size());
});

gulp.task('clean', function () {
  return gulp.src(['.tmp', DIST], {read: false}).pipe($.rimraf({force: true}));
});

gulp.task('build', ['html', 'partials', 'images', 'fonts']);
