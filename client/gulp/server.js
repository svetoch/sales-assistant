'use strict';

var gulp = require('gulp');

var browserSync = require('browser-sync');
var proxy = require('proxy-middleware');
var url = require('url');

var proxyOptions = url.parse('http://localhost:8888/api');
proxyOptions.route = '/api';

function browserSyncInit(baseDir, files, browser) {
  browser = browser === undefined ? 'default' : browser;

  browserSync.instance = browserSync.init(files, {
    reloadDelay: 1000,
    notify: false,
    startPath: '/index.html',
    server: {
      baseDir: baseDir,
      middleware: proxy(proxyOptions)
    },
    browser: browser
  });
}

gulp.task('serve', ['watch'], function () {
  browserSyncInit([
    'src',
    '.tmp'
  ], [
    'src/*.html',
    '.tmp/styles/**/*.css',
    'src/app/**/*.js',
    'src/app/**/*.html',
    'src/images/**/*'
  ]);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit('dist');
});

gulp.task('serve:e2e', function () {
  browserSyncInit(['app', '.tmp'], null, []);
});

gulp.task('serve:e2e-dist', ['watch'], function () {
  browserSyncInit('dist', null, []);
});
