CREATE TABLE CATEGORY
(
  ID INTEGER auto_increment PRIMARY KEY NOT NULL,
  DISCOUNT DOUBLE,
  NAME VARCHAR(255) NOT NULL UNIQUE
);
CREATE TABLE CLIENT
(
  ID VARCHAR(255) PRIMARY KEY NOT NULL,
  DATE_OF_BIRTH TIMESTAMP,
  DISCOUNT INTEGER,
  EMAIL VARCHAR(255),
  NAME VARCHAR(255) NOT NULL,
  TELEPHONE VARCHAR(255)
);
CREATE TABLE CONTRACTOR
(
  ID INTEGER auto_increment PRIMARY KEY NOT NULL,
  NAME VARCHAR(255) NOT NULL
);
CREATE TABLE INVOICE
(
  ID VARCHAR(20) PRIMARY KEY NOT NULL,
  CONTRACTOR INT,
  CREATION TIMESTAMP NOT NULL,
  constraint fk_invoice_user foreign key(CONTRACTOR) references CONTRACTOR(id),
);
CREATE TABLE INVOICE_ITEM
(
  ID BIGINT auto_increment  PRIMARY KEY NOT NULL,
  AMOUNT INTEGER,
  PRICE DOUBLE,
  PRODUCT VARCHAR(255)
);
CREATE TABLE INVOICE_ITEMS
(
  INVOICE VARCHAR(20) NOT NULL,
  ITEMS BIGINT NOT NULL
);
CREATE TABLE PRODUCT
(
  ID VARCHAR(255) PRIMARY KEY NOT NULL,
  AMOUNT INTEGER NOT NULL,
  NAME VARCHAR(255),
  PRICE DOUBLE NOT NULL,
  BY_WEIGHT BOOLEAN,
  HIDDEN BOOLEAN,
  INCOME_PRICE DOUBLE,
  CATEGORY INTEGER,
  PRODUCT_DISCOUNT INTEGER
);
CREATE TABLE PRODUCT_DISCOUNT
(
  ID INTEGER auto_increment PRIMARY KEY NOT NULL,
  BULK_AMOUNT INTEGER DEFAULT -1,
  BULK_PRICE INTEGER DEFAULT -1,
  DISCOUNT INTEGER DEFAULT -1
);
CREATE TABLE SALE
(
  ID BIGINT auto_increment PRIMARY KEY NOT NULL,
  ORDER_TIME TIMESTAMP,
  PAYMENT_METHOD VARCHAR(255) NOT NULL,
  TOTAL DOUBLE,
  CLIENT VARCHAR(255)
);
CREATE TABLE SALE_ITEMS
(
  SALE BIGINT NOT NULL,
  ITEMS BIGINT NOT NULL
);
CREATE TABLE SOLD_ITEM
(
  ID BIGINT auto_increment PRIMARY KEY NOT NULL,
  AMOUNT INTEGER NOT NULL,
  NAME VARCHAR(255),
  PRICE DOUBLE NOT NULL,
  DISCOUNT DOUBLE,
  DISCOUNT_TYPE VARCHAR(255),
  ORDER_TIME TIMESTAMP,
  ORIGINAL VARCHAR(255)
);
ALTER TABLE INVOICE_ITEM ADD FOREIGN KEY ( PRODUCT ) REFERENCES PRODUCT ( ID );
CREATE INDEX FK_INVOICE_ITEM_PRODUCT  ON INVOICE_ITEM ( PRODUCT );
ALTER TABLE INVOICE_ITEMS ADD FOREIGN KEY ( INVOICE ) REFERENCES INVOICE ( ID );
ALTER TABLE INVOICE_ITEMS ADD FOREIGN KEY ( ITEMS ) REFERENCES INVOICE_ITEM ( ID );
CREATE UNIQUE INDEX UK_ITEMS ON INVOICE_ITEMS ( ITEMS );
CREATE INDEX FK_INVOICE ON INVOICE_ITEMS ( INVOICE );
ALTER TABLE PRODUCT ADD FOREIGN KEY ( CATEGORY ) REFERENCES CATEGORY ( ID );
ALTER TABLE PRODUCT ADD FOREIGN KEY ( PRODUCT_DISCOUNT ) REFERENCES PRODUCT_DISCOUNT ( ID );
CREATE INDEX FK_PRODUCT_CATEGORY ON PRODUCT ( CATEGORY );
CREATE INDEX FK_PRODUCT_DISCOUNT ON PRODUCT ( PRODUCT_DISCOUNT );
ALTER TABLE SALE ADD FOREIGN KEY ( CLIENT ) REFERENCES CLIENT ( ID );
CREATE INDEX FK_SALE_CLIENT ON SALE ( CLIENT );
ALTER TABLE SALE_ITEMS ADD FOREIGN KEY ( SALE ) REFERENCES SALE ( ID );
ALTER TABLE SALE_ITEMS ADD FOREIGN KEY ( ITEMS ) REFERENCES SOLD_ITEM ( ID );
CREATE UNIQUE INDEX UK_SALE_ITEMS_ITEM ON SALE_ITEMS ( ITEMS );
CREATE INDEX FK_SALE_ITEMS_SALE ON SALE_ITEMS ( SALE );
ALTER TABLE SOLD_ITEM ADD FOREIGN KEY ( ORIGINAL ) REFERENCES PRODUCT ( ID );
CREATE INDEX FK_SOLD_ITEM_PRODUCT ON SOLD_ITEM ( ORIGINAL );

CREATE VIEW Advanced_report AS
  SELECT DISTINCT
    product.id,
    product.name,
    sold_item.discount_type,
    sold_item.discount,
    SUM(DISTINCT invoice_item.amount) AS invoice_amount,
    SUM(DISTINCT sold_item.amount)    AS sold_amount,
    invoice_item.price              invoice_price,
    sold_item.price                sold_price,
    COUNT(DISTINCT sold_item.id)      AS sold_frequency,
    TRUNCATE(order_time, 0) as order_date
  FROM product
    JOIN invoice_item
      ON invoice_item.product = product.id
    JOIN sold_item
      ON sold_item.original = product.id
  GROUP BY product.id, discount_type, discount, invoice_item.price, sold_item.price, order_date;

CREATE VIEW Daily_Proceed AS
  SELECT
    payment_method,
    sum(total) AS sum,
    TRUNCATE(order_time, 0) as order_date
  FROM sale
  GROUP BY order_date, payment_method;

CREATE VIEW Daily_report_item AS
  SELECT
    TRUNCATE(order_time, 0) as order_date,
    PRODUCT.name,
    SUM(SOLD_ITEM.amount)                   AS num,
    SUM(SOLD_ITEM.amount * SOLD_ITEM.price) AS sum,
    count(DISTINCT SOLD_ITEM.id)         AS frequency
  FROM SOLD_ITEM
    JOIN product ON SOLD_ITEM.original = PRODUCT.id
  GROUP BY product.name,  order_date;




