'use strict';

angular.module('salesApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'restangular'
])
  .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "RestangularProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, RestangularProvider) {
    $urlRouterProvider
      .otherwise('/products');

    RestangularProvider.addResponseInterceptor(function (data, op) {
      if (op === 'getList' && !angular.isArray(data)) {
        return data.content;
      }
      return data;
    });
    RestangularProvider.setBaseUrl('/api');
    //$locationProvider.html5Mode(true);
  }]);

'use strict';

angular.module('salesApp').controller('AddInvoiceCtrl', ["$scope", "$state", "Invoice", "Product", function ($scope, $state, Invoice, Product) {
  function create() {
    $scope.invoice = Invoice.create();
    $scope.newItem = Invoice.createItem();
  }
  create();

  $scope.addItem = function (invoice, item) {
    Invoice.addItem(invoice, item);
    $scope.newItem = Invoice.createItem();
  };

  $scope.notFound = false;
  $scope.productNames = [];
  function findByName(val) {
    if (val.length > 2) {
      Invoice.findByProductName(val, $scope.newItem).then(function (productNames) {
        $scope.notFound = false;
        $scope.productNames = productNames;
      }).catch(function () {
          $scope.notFound = true;
      });
    } else {
      $scope.notFound = false;
    }
  }
  $scope.$watch('newItem.product.name', findByName);

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = !$scope.opened;
  };

  $scope.newProduct = function (id) {
    Product.showEdit({id: id}, true).then(function (product) {
      findByName(product.id);
    });
  };

  $scope.save = function (invoice) {
    if ($scope.newItem.product.id && !$scope.notFound) {
      Invoice.addItem(invoice, $scope.newItem);
    }
    Invoice.save(invoice).then(function () {
      $state.go('main.invoices');
    });
  };
}]);

(function() {
    'use strict';
    angular.module('salesApp').controller('InvoicesCtrl', ["$scope", "Invoice", function($scope,
        Invoice) {
        $scope.invoices = Invoice.invoices;
        $scope.remove = function(invoice) {
            Invoice.remove(invoice).then(function() {
                $scope.invoices = Invoice.invoices;
            });
        }
    }]);
})();

'use strict';

angular.module('salesApp')
  .config(["$stateProvider", function ($stateProvider) {
    $stateProvider
      .state('main.invoices', {
        url: '/products/invoices',
        templateUrl: 'app/products/invoices/invoices.html',
        controller: 'InvoicesCtrl'
      })
      .state('main.add-invoice', {
        url: '/products/invoices/add',
        templateUrl: 'app/products/invoices/add/add-invoice.html',
        controller: 'AddInvoiceCtrl'
      });
  }])
  .factory('Invoice', ["Restangular", "$modal", "Product", "$q", function (Restangular, $modal, Product, $q) {
    var Invoices = Restangular.all('invoices');

    function fetchInvoices() {
      var promise = Invoices.getList();
      InvoiceService.invoices = promise.$object;
      promise.then(function (invoices) {
        invoices.forEach(function (invoice) {
          invoice.getList('items').then(function (items) {
            invoice.items = items;
          });
        })
      });
    }

    var InvoiceService = {
      findByProductName: function (name, item) {
        var $p = Product.getListByName(name);
        return $p.then(function (products) {
          if (!products.length) {
            return $q.reject();
          }

          if (products.length === 1) {
            item.product = products[0];
          }
          return _.map(products, function (p) {
            return p.name;
          });
        });
      },
      save: function (invoice) {
        invoice.date = moment(new Date(invoice.date)).format('DD.MM.YYYY');
        invoice.items = _.map(invoice.items, function (item) {
          item.product = _.pick(item.product, 'id');
          return _.pick(item, 'product', 'amount', 'price');
        });
        return Invoices.post(invoice).then(fetchInvoices);
      },
      remove: function (invoice) {
        return invoice.remove().then(fetchInvoices);
      },
      createItem: function () {
        return {
          product: {id: '', name: '', price: 0},
          price: 0,
          amount: 0,
          incomePrice: 0
        };
      },
      create: function () {
        return {
          code: '',
          date: Date.now(),
          items: []
        }
      },
      addItem: function (invoice, item) {
        invoice.items.push(item);
      },
      showAdd: function () {
        $modal.open({
          templateUrl: 'app/products/invoices/add/add-invoice.html',
          controller: 'AddInvoiceCtrl'
        });
      }
    };

    fetchInvoices();

    return InvoiceService;
  }]);

angular.module('salesApp').controller('EditProductCtrl', ["$scope", "$modalInstance", "Product", "product", "Category", "add", function ($scope, $modalInstance, Product, product, Category, add) {
  $scope.product = product;
  $scope.add = add;
  $scope.categories = Category.categories;
  $scope.save = function () {
    Product.save($scope.product, add).then(function (product) {
      $modalInstance.close(product);
    });
  };
}]);

angular.module('salesApp').controller('EditCategoryCtrl', ["$scope", "Category", "category", "add", function ($scope, Category, category, add) {
  $scope.category = category;
  $scope.save = function (category) {
    Category.save(category, add).then(function () {
      $scope.$close();
    })
  };
}]);

'use strict';

angular.module('salesApp').factory('Category', ["$modal", "Restangular", function ($modal, Restangular) {
  var Category = Restangular.all('products/categories');

  function fetchCategories() {
    CategoryService.categories = Category.getList().$object;
  }

  var CategoryService = {
    current: null,
    showEdit: function (category) {

      var add = !category;
      category = category || CategoryService.create();

      var modal = $modal.open({
        templateUrl: 'app/products/categories/category-edit.html',
        controller: 'EditCategoryCtrl',
        resolve: {
          add: function () {
            return add;
          },
          category: function () {
            return category;
          }
        }
      });
      return modal.result.then(fetchCategories);
    },
    save: function (category, add) {
      return add ? Category.post(category) : category.put();
    },
    create: function () {
      return {name: ''};
    }
  };

  fetchCategories();

  return CategoryService;
}]);

angular.module('salesApp').directive('navbar', function () {
  'use strict';
  return {
    restrict: 'E',
    templateUrl: 'app/components/navbar/navbar.html'
  };
});

angular.module('salesApp').factory('Msg', ["$modal", function ($modal) {
  return function (msg, title) {
    return $modal.open({
      templateUrl: 'app/components/msg/msg.html',
      controller: ["$scope", function ($scope) {
        $scope.msg = msg;
        $scope.title = title;
      }]
    });
  }
}]);

'use strict';

angular.module('salesApp').directive('gridPanel', function () {
  return {
    restrict: 'E',
    transclude: true,
    link: function (scope) {
      scope.setActive = function (option) {
        scope.activeOption = option;
      }
    },
    templateUrl: 'app/components/grid-panel/grid-panel.html'
  };
});

(function () {
  'use strict';

  angular.module('salesApp')
    .config(["$stateProvider", function ($stateProvider) {
      $stateProvider
        .state('main.reports', {
          url: '/reports',
          templateUrl: 'app/reports/reports.html',
          controller: 'ReportCtrl'
        });
    }])
    .factory('Report', ["Restangular", function (Restangular) {
      var ReportService = {
      };

      var AdvancedReport = Restangular.all('report/advanced'),
        DailyReport = Restangular.all('report/daily/' + moment(new Date()).format('YYYY-MM-DD'));

      function fetchReports() {
        ReportService.dailyReports = DailyReport.getList().$object;
        ReportService.advancedReports = AdvancedReport.getList().$object;
      }
      fetchReports();
      return ReportService;
    }]);

})();

(function () {
  'use strict';

  angular.module('salesApp').controller('ReportCtrl', ["$scope", "Report", "Sale", function ($scope, Report, Sale) {
    $scope.advancedReports = Report.advancedReports;
    $scope.sales = Sale.sales;
  }]);
})();

(function () {
  'use strict';

  angular.module('salesApp')
    .config(["$stateProvider", function ($stateProvider) {
      $stateProvider
        .state('main.products', {
          url: '/products',
          templateUrl: 'app/products/products.html',
          controller: 'ProductCtrl'
        });
    }])
    .factory('Product', ["Restangular", "$modal", "Category", function (Restangular, $modal, Category) {
      var RestProducts = Restangular.all('products');
      RestProducts.addRestangularMethod('lowAmount', 'getList', undefined, {filter: 'criticalAmount'});

      var params = {filter: []};

      function fetchProducts() {
        ProductService.products = RestProducts.getList(params).$object;
        ProductService.lowProducts = RestProducts.lowAmount().$object;
      }

      var ProductService = {
        filterByCategory: function (category) {
          ProductService.filter('category', category ? category.id : null);
        },
        filter: function (name, val) {
          params.filter.forEach(function (e, i, a) {
            if (e.indexOf(name) == 0) {
              a.splice(i, 1);
            }
          });

          if (val) {
            params.filter.push(name + ':' + val);
          }

          fetchProducts();
        },
        getListByName: function (name) {
          return RestProducts.getList({filter: ['name:' + name]});
        },
        get: function (id) {
          return RestProducts.one(id).get();
        },
        remove: function (product) {
          return product.remove().then(fetchProducts);
        },
        save: function (product, add) {
          product.category = _.pick(product.category, 'id');
          return add ? RestProducts.post(product) : product.put();
        },
        showEdit: function (product, add) {
          add = (add === undefined) ? !product : add;
          product = product || ProductService.create();

          var modal = $modal.open({
            templateUrl: 'app/products/edit/product-edit.html',
            controller: 'EditProductCtrl',
            resolve: {
              add: function () {
                return add;
              },
              product: function () {
                return product;
              }
            }
          });
          var result = modal.result;
          result.then(fetchProducts);
          return result;
        },
        create: function () {
          return {};
        }
      };

      fetchProducts();

      return ProductService;
    }]);
})();




'use strict';

angular.module('salesApp')
  .controller('ProductCtrl', ["$scope", "$modal", "Product", "Category", "Invoice", function ($scope, $modal, Product, Category, Invoice) {
    function fetchProducts() {
      $scope.products = Product.products;
      $scope.lowProducts = Product.lowProducts;
    }
    fetchProducts();

    $scope.searchOptions = [{name: 'name', label: 'Наименование'}];
    $scope.activeOption = $scope.searchOptions[0];
    $scope.searchText = '';
    $scope.$watch('searchText', function (val) {
      Product.filter($scope.activeOption.name, val);
      fetchProducts();
    });

    $scope.showAddInvoice = Invoice.showAdd;
    $scope.remove = function (product) {
      Product.remove(product).then(fetchProducts);
    };
    $scope.edit = function (product) {
      Product.showEdit(product).then(fetchProducts);
    };
    $scope.add = function () {
      Product.showEdit().then(fetchProducts);
    };
    $scope.showLow = false;
    $scope.$watch('showLow', function (val) {
      if (val) {
        $scope.products = Product.lowProducts;
      } else {
        $scope.products = Product.products;
      }
    });

    function fetchCategories() {
      $scope.categories = Category.categories;
      $scope.currentCat = Category.current;
    }
    fetchCategories();

    $scope.setCategory = function (category) {
      Category.current = category;
      $scope.currentCat = category;
      Product.filterByCategory(category);
      fetchProducts();
    };
    $scope.editCategory = function (category) {
      Category.showEdit(category).then(fetchCategories);
    };
  }]);

'use strict';

angular.module('salesApp')
  .config(["$stateProvider", function ($stateProvider) {
    $stateProvider
      .state('main.order', {
        url: '/order',
        templateUrl: 'app/order/order.html',
        controller: 'OrderCtrl'
      });
  }])
  .factory('Sale', ["Restangular", "$q", "Product", function (Restangular, $q, Product) {
    var DiscountType = {NONE: 'NONE', PRODUCT: 'PRODUCT', CUSTOM: 'CUSTOM', BULK: 'BULK'};

    Restangular.extendModel('sales', function (model) {
      model.items = [];
      model.save = function () {
        var m = _.clone(model);
        m.items = _.map(m.items, function (i) {
          return _.omit(i, 'product');
        });
        m.client = _.pick(m.client, 'id');

        return m.post();
      };
      model.addItem = function (itemId) {
        var item = _.find(model.items, function (i) {
          return i.original.id === itemId;
        });

        var promise;
        if (item) {
          item.amount++;
          var d = $q.defer();
          d.resolve();
          promise = d.promise;
        } else {
          promise = Product.get(itemId).then(function (product) {
            model.items.push({
              discountType: DiscountType.NONE,
              discount: product.discount.discount,
              original: {id: product.id},
              product: product,
              amount: 1,
              name: product.name
            });
          });
        }

        promise.then(model.updateTotals);
        return promise;
      };
      model.removeItem = function (item) {
        var idx = model.items.indexOf(item);
        model.items.splice(idx, 1);
        model.updateTotals();
      };
      model.updateTotals = function () {
        model.items.forEach(function (i) {
          if (i.isBulk) {
            i.discountType = DiscountType.BULK;
            i.price = i.product.discount.bulkPrice;
            i.total = i.amount * i.price;
          } else {
            if (i.discount == i.product.discount.discount) {
              i.discountType = DiscountType.PRODUCT;
            } else {
              i.discountType = DiscountType.CUSTOM;
            }

            i.price = i.product.price;
            i.total = i.amount * i.price * (100 - i.discount) * 0.01
          }
        });
        model.subTotal = model.items.reduce(function (m, i) {
          return m + i.amount * i.product.price;
        }, 0);
        model.total = model.items.reduce(function (m, i) {
          return m + i.amount * i.product.price * (100 - i.discount) * 0.01;
        }, 0);
      };
      return model;
    });

    function fetchSales() {
      var promise = Restangular.all('sales').getList();
      SaleService.sales = promise.$object;
      promise.then(function (sales) {
        sales.forEach(function (sale) {
          sale.getList('items').then(function (items) {
            sale.items = items;
          });
        });
      });
    }

    var SaleService = {
      DiscountType: DiscountType,

      create: function () {
        var sale = Restangular.one('sales');
        sale.paymentMethod = 'CASH';
        return sale;
      }
    };
    fetchSales();

    return SaleService;
  }]);

'use strict';

angular.module('salesApp').controller('OrderCtrl', ["$scope", "Msg", "Product", "Client", "Sale", function ($scope, Msg, Product, Client, Sale) {
  function emptyText() {
    $scope.notFound = null;
    $scope.searchText = '';
  }

  emptyText();


  $scope.searchOptions = [{name: 'id', label: 'Штрих-код'}];
  $scope.activeOption = $scope.searchOptions[0];
  $scope.sale = Sale.create();
  $scope.discountTypes = Sale.discountType;

  $scope.save = function () {
    $scope.sale.save().then(function () {
      $scope.sale = Sale.create();
      Msg('Покупка успешно совершена');
    }).catch(function () {
      Msg('Не удалось совершить покупку');
    });
  };

  $scope.$watch('searchText', function (val) {
    if (val.length < 10) {
      if (!val.length) $scope.notFound = false;
      return;
    }

    if (val.indexOf('CLI') !== -1) {
      var $promise = Client.one(val).get();
      $promise.then(emptyText);
      $scope.sale.client = $promise.$object;
    } else {
      $scope.sale.addItem(val).then(emptyText).catch(function () {
        $scope.notFound = val;
      });
    }
  });
}]).controller('OrderItemCtrl', ["$scope", function ($scope) {
  $scope.$watch('item.discount', $scope.sale.updateTotals);
  $scope.$watch('item.amount', $scope.sale.updateTotals);
  $scope.$watch('item.isBulk', $scope.sale.updateTotals);
}]);

(function () {
  'use strict';

  angular.module('salesApp').directive('discountType', function () {
    var Icons = {
      PRODUCT: 'glyphicon-tasks',
      CATEGORY: ''
    },
      defaultIcon = 'glyphicon-pencil';

    return {
      restrict: 'AE',
      scope: {
        discountType: '=type'
      },
      link: function (scope) {
        scope.iconClass = Icons[scope.discountType] || defaultIcon;
      },
      template: '<span class="glyphicon" ng-class="iconClass"></span>'
    }
  })
})();

'use strict';

angular.module('salesApp')
  .config(["$stateProvider", function ($stateProvider) {
    $stateProvider
      .state('main', {
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });
  }]);

'use strict';

angular.module('salesApp')
  .controller('MainCtrl', function () {
  });

'use strict';

angular.module('salesApp')
  .config(["$stateProvider", function ($stateProvider) {
    $stateProvider
      .state('main.clients', {
        url: '/clients',
        templateUrl: 'app/clients/clients.html',
        controller: 'ClientsCtrl'
      });
  }])
  .factory('Client', ["Restangular", function (Restangular) {
    return Restangular.all('clients');
  }]);

'use strict';

angular.module('salesApp').controller('ClientsCtrl', ["$scope", "$modal", "Client", function ($scope, $modal, Client) {
  $scope.searchOptions = [{name: 'name', label: 'ФИО'}];
  $scope.searchText = '';
  $scope.activeOption = $scope.searchOptions[0];
  $scope.clients = Client.getList().$object;
  $scope.edit = function (client) {
    var modal = $modal.open({
      templateUrl: 'app/clients/client-edit.html',
      resolve: {
        client: function () {
          return client;
        }
      },
      controller: ["$scope", "client", function ($scope, client) {
        $scope.client = client;
      }]
    });
    modal.result.then(function (client) {
      client.put();
      $scope.clients = Client.getList().$object;
    })
  };
  $scope.remove = function (client) {
    client.remove();
    $scope.clients = Client.getList().$object;
  };
}]);

!function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/clients/client-edit.html",'<div class="modal-header"><button type="button" class="close" ng-click="$dismiss()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> {{client.name}}</h4></div><div class="modal-body"><form role="form" class="form-horizontal"><div class="form-group"></div><div class="form-group"><label for="input-barcode" class="col-sm-3 control-label">Штрих-код:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-barcode" ng-model="client.id" disabled=""></div></div><div class="form-group"><label for="input-name" class="col-sm-3 control-label">ФИО:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-name" ng-model="client.name"></div></div><div class="form-group"><label for="input-dateOfBirth" class="col-sm-3 control-label">Дата рождения:</label><div class="col-sm-9"><input type="datetime" class="form-control" id="input-dateOfBirth" ng-model="client.dateOfBirth"></div></div><div class="form-group"><label for="input-email" class="col-sm-3 control-label">E-mail:</label><div class="col-sm-9"><input type="email" class="form-control" id="input-email" ng-model="client.email"></div></div><div class="form-group"><label for="input-telephone" class="col-sm-3 control-label">Телефон:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-telephone" ng-model="client.telephone"></div></div></form></div><div class="modal-footer"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-click="$dismiss()">Отменить</button><button type="button" class="btn btn-primary" ng-click="$close(client)">Сохранить</button></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/clients/clients.html",'<div class="row"><div class="col-xs-12"><grid-panel><table class="table"><thead><tr><th>Штрих-код</th><th>ФИО</th><th>Дата рождения</th><th>Номер телефона</th><th>E-mail</th><th></th><th></th></tr></thead><tbody><tr ng-repeat="client in clients"><td>{{client.id}}</td><td>{{client.name}}</td><td>{{client.dateOfBirth}}</td><td>{{client.telephone}}</td><td>{{client.email}}</td><td><button class="btn btn-default btn-sm" ng-click="edit(client)"><span class="glyphicon glyphicon-edit"></span></button></td><td><button class="btn btn-danger btn-sm" ng-click="remove(client)"><span class="glyphicon glyphicon-trash"></span></button></td></tr></tbody></table></grid-panel></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/main/main.html",'<navbar></navbar><div class="container-fluid" ui-view=""></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/order/order.html",'<div class="row"><div class="col-xs-8"><alert ng-show="notFound" type="warning"><p>Товар с штрих-кодом {{notFound}} не найден.</p></alert><div class="panel panel-default"><div class="panel-heading"><div class="input-group input-group-lg"><div class="input-group-btn"><button class="btn btn-default dropdown-toggle" dropdown="" type="button"><span class="glyphicon glyphicon-barcode" ng-if="activeOption.name === \'id\'"></span> <span ng-hide="activeOption.name === \'id\'">{{activeOption.label}}</span> <span class="caret" ng-show="searchOptions.length > 1"></span></button><ul class="dropdown-menu"><li ng-repeat="option in searchOptions" ng-click="setActive(option)">{{option.label}}</li></ul></div><input type="text" class="form-control" ng-model="searchText"><div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div></div></div><div class="panel-body" ng-hide="sale.client || sale.items.length"><h3>Чтобы начать, отсканируйте товар или карту клиента</h3></div><table class="table" ng-show="sale.items.length"><thead><tr><th>Наименование</th><th>Количество</th><th>Цена, р</th><th>Подитог, р</th><th>Скидка, %</th><th></th></tr></thead><tbody><tr ng-hide="sale.items.length || sale.client"><td colspan="4"></td></tr><tr ng-repeat="item in sale.items" ng-controller="OrderItemCtrl"><td>{{item.product.name}}</td><td width="15%"><div class="input-group"><input type="number" class="form-control" id="input-item-amount" ng-model="item.amount"><span class="input-group-addon">{{product.byWeight ? \'кг\' : \'шт\'}}</span></div></td><td>{{item.price | number:2}}</td><td>{{item.total | number:2}}</td><td width="15%"><div class="input-group"><label class="sr-only" for="input-item-discount">Скидка</label><input type="number" ng-model="item.discount" id="input-item-discount" class="form-control" ng-disabled="item.isBulk"><span class="input-group-addon" discount-type=""></span></div></td><td><button class="btn btn-default btn-sm" btn-checkbox="" ng-model="item.isBulk">Опт</button></td><td><button class="btn btn-danger btn-sm" ng-click="sale.removeItem(item)"><span class="glyphicon glyphicon-trash"></span></button></td></tr></tbody><tfoot><tr><td>ИТОГО</td><td colspan="5">{{sale.subTotal | number:2}}</td></tr><tr><td>ИТОГО, СО СКИДКОЙ</td><td colspan="5">{{sale.total | number:2}}</td></tr></tfoot></table><div class="panel-footer"><div class="btn-toolbar"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-click="cancel()"><span class="glyphicon glyphicon-ban-circle"></span> Отменить</button><button type="button" class="btn btn-primary" ng-class="{disabled: !sale.items.length || !sale.client}" ng-click="save()"><span class="glyphicon glyphicon-shopping-cart"></span> Сохранить</button></div><div class="btn-group btn-group-sm"><button class="btn btn-default" ng-model="sale.paymentMethod" btn-radio="\'CASH\'">Наличные</button><button class="btn btn-default" ng-model="sale.paymentMethod" btn-radio="\'CARD\'">Карта</button></div></div></div></div></div><div class="col-xs-4"><div class="panel panel-default" ng-if="sale.client"><div class="panel-heading"><strong>Клиент:</strong>{{sale.client.name}}</div><ul class="list-group"><li class="list-group-item"><strong>Дата рождения:</strong>{{sale.client.dateOfBirth}}</li><li class="list-group-item"><strong>Скидка:</strong>{{sale.client.discount}}%</li></ul></div></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/products/products.html",'<div class="row"><div class="col-sm-3"><div class="panel panel-default"><div class="panel-heading">Категории</div><div class="list-group"><a class="list-group-item" ng-class="{active: currentCat === null}" href="" ng-click="setCategory(null)">Все категории</a> <a ng-repeat="cat in categories" class="list-group-item" ng-class="{active: cat.id === currentCat.id}" ng-click="setCategory(cat)"><div class="row"><div class="col-sm-1"><span class="badge">{{cat.productCount}}</span></div><div class="col-sm-8">{{cat.name}}</div><div class="col-sm-2"><button type="button" class="btn btn-default btn-xs" ng-click="editCategory(cat)"><span class="glyphicon glyphicon-edit"></span></button></div></div></a></div><div class="panel-footer"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default btn-sm" ng-click="editCategory()"><span class="glyphicon glyphicon-plus"></span> Добавить</button></div></div></div></div><div class="col-sm-9"><div class="row voffset-15"><div class="col-xs-12"><div class="btn-toolbar" role="toolbar"><div class="btn-group btn-group-sm" ng-show="lowProducts.length"><button type="button" class="btn btn-warning" btn-checkbox="" ng-model="showLow"><span class="glyphicon glyphicon-bell"></span> Товар заканчивается ({{lowProducts.length | number}})</button></div><div class="btn-group btn-group-sm pull-right"><a type="button" class="btn btn-default" href="#/products/invoices/add"><span class="glyphicon glyphicon-file"></span> Новая накладная</a> <a class="btn btn-default" href="#/products/invoices"><span class="glyphicon glyphicon-list-alt"></span> Показать накладные</a></div></div></div></div><grid-panel><table class="table"><thead><tr><th>Штрих-код</th><th>Наименование</th><th>Количество</th><th>Цена (Приход)</th><th>Цена (Опт)</th><th>Цена (Розница)</th><th>Скидка (%)</th><th></th><th></th></tr></thead><tbody><tr ng-repeat="product in products"><td>{{product.id}}</td><td>{{product.name}}</td><td>{{product.amount}} {{product.byWeight ? \'кг\' : \'шт\'}}</td><td>{{product.incomePrice | number:2}}</td><td>{{product.discount.bulkPrice | number:2}}</td><td>{{product.price | number:2}}</td><td>{{product.discount.discount}}</td><td><button class="btn btn-default btn-sm" ng-click="edit(product)"><span class="glyphicon glyphicon-edit"></span></button></td><td><button class="btn btn-danger btn-sm" ng-click="remove(product)"><span class="glyphicon glyphicon-trash"></span></button></td></tr></tbody></table></grid-panel></div><div class="col-sm-2"></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/reports/reports.html",'<div class="row"><div class="col-sm-12"><div class="panel panel-default"><div class="panel-heading"><h4>Отчеты</h4></div><div class="panel-body"><tabset><tab heading="Продажи"><table class="table"><thead><tr><th width="10%">Время</th><th width="10%">Сумма, р</th><th width="10%">Вид оплаты</th><th width="15%">Клиент</th><th>Товары</th></tr></thead><tbody><tr ng-repeat="sale in sales"><td>{{sale.orderTime}}</td><td>{{sale.total | number:2}}</td><td>{{sale.paymentMethod === \'CASH\' ? \'Наличные\' : \'Карта\'}}</td><td>{{sale.client.name}}</td><td><accordion><accordion-group heading="Показать ({{sale.items.length}})"><table class="table"><thead><tr><th>Наименование</th><th>Цена, р</th><th>Количество</th><th>Скидка, %</th><th>Сумма, р</th></tr></thead><tbody><tr ng-repeat="item in sale.items"><td>{{item.original.name}}</td><td>{{item.price | number:2}}</td><td>{{item.amount}}</td><td>{{item.discount}}</td><td>{{item.total | number:2}}</td></tr></tbody></table></accordion-group></accordion></td></tr></tbody></table></tab><tab heading="За день"><table></table></tab><tab heading="Детальный"><table class="table"><thead><tr><th>Наименование</th><th>Куплено</th><th>Цена покупки, р</th><th>Продано</th><th>Цена продажи, р</th><th>Скидка, %</th><th>Сумма продажи, р</th><th>Прибыль, р</th></tr></thead><tbody><tr ng-repeat="report in advancedReports"><td>{{report.productName}}</td><td>{{report.invoiceAmount}}</td><td>{{report.invoicePrice | number:2}}</td><td>{{report.soldAmount}}</td><td>{{report.soldPrice | number:2}}</td><td>{{report.discount}}<discount-type type="report.discountType"></discount-type></td><td>{{report.soldSumPrice | number:2}}</td><td>{{report.profit | number:2}}</td></tr></tbody></table></tab></tabset></div></div></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/components/grid-panel/grid-panel.html",'<div class="panel panel-default"><div class="panel-heading"><div class="input-group input-group-lg"><div class="input-group-btn"><button class="btn btn-default dropdown-toggle" dropdown="" type="button"><span class="glyphicon glyphicon-barcode" ng-if="activeOption.name === \'id\'"></span> <span ng-hide="activeOption.name === \'id\'">{{activeOption.label}}</span> <span class="caret" ng-show="searchOptions.length > 1"></span></button><ul class="dropdown-menu"><li ng-repeat="option in searchOptions" ng-click="setActive(option)">{{option.label}}</li></ul></div><input type="text" class="form-control" ng-model="searchText"><div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div></div></div><ng-transclude></ng-transclude><div class="panel-footer"><div class="btn-toolbar js-actions"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-if="cancel" ng-click="cancel()"><span class="glyphicon glyphicon-ban-circle"></span> Отменить</button><button type="button" class="btn btn-primary" ng-if="save" ng-click="save()"><span class="glyphicon glyphicon-shopping-cart"></span> Сохранить</button></div><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-if="add" ng-click="add()"><span class="glyphicon glyphicon-plus"></span> Добавить</button></div></div></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/components/msg/msg.html",'<div class="modal-header"><h3 class="modal-title">{{title}}</h3></div><div class="modal-body"><p>{{msg}}</p></div><div class="modal-footer"><button class="btn btn-primary btn-sm" ng-click="$close()">OK</button></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/components/navbar/navbar.html",'<nav class="navbar navbar-inverse"><div class="container-fluid"><div class="navbar-header"><a href="#" class="navbar-brand">Sales Assistant</a></div><div class="collapse navbar-collapse"><ul class="nav navbar-nav navbar-left"><li><a href="#/order"><span class="glyphicon glyphicon-shopping-cart"></span> Продажа</a></li><li><a href="#/clients"><span class="glyphicon glyphicon-user"></span> Клиенты</a></li><li><a href="#/reports"><span class="glyphicon glyphicon-list-alt"></span> Отчет</a></li><li><a href="#/products"><span class="glyphicon glyphicon-tasks"></span> Товары</a></li><li><a href="#/discounts"><span class="glyphicon glyphicon-gift"></span> Скидки</a></li></ul><ul class="nav navbar-nav navbar-right"><li><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></li><li><a href="#"><span class="glyphicon glyphicon-cog"></span></a></li><li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li></ul></div></div></nav>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/products/categories/category-edit.html",'<div class="modal-header"><button type="button" class="close" ng-click="$dismiss()"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Категория</h4></div><div class="modal-body"><form role="form" class="form-horizontal" novalidate=""><div class="form-group"></div><div class="form-group"><label for="input-name" class="col-sm-3 control-label">Название:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-name" ng-model="category.name"></div></div><div class="form-group"><label for="input-discount" class="col-sm-3 control-label">Скидка:</label><div class="col-sm-9"><div class="input-group"><input type="number" class="form-control" id="input-discount" ng-model="category.discount"><span class="input-group-addon">%</span></div></div></div></form></div><div class="modal-footer"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-click="$dismiss()">Отменить</button><button type="button" class="btn btn-primary" ng-click="save(category)">Сохранить</button></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/products/edit/product-edit.html",'<div class="modal-header"><button type="button" class="close" ng-click="$dismiss()"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> {{product.name}}</h4></div><div class="modal-body"><tabset><tab heading="Основное"><form role="form" class="form-horizontal" novalidate=""><div class="form-group"></div><div class="form-group"><label for="input-barcode" class="col-sm-3 control-label">Штрих-код:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-barcode" ng-model="product.id" ng-disabled="!add"></div></div><div class="form-group"><label for="input-category" class="col-sm-3 control-label">Категория:</label><div class="col-sm-9"><select class="form-control" id="input-category" ng-model="product.category" ng-options="cat.name for cat in categories"></select></div></div><div class="form-group"><label for="input-name" class="col-sm-3 control-label">Наименование:</label><div class="col-sm-9"><input type="text" class="form-control" id="input-name" ng-model="product.name"></div></div><div class="form-group"><label for="input-amount" class="col-sm-3 control-label">Количество:</label><div class="col-sm-9"><input type="number" class="form-control" id="input-amount" ng-model="product.amount"></div></div><div class="form-group"><div class="col-sm-offset-3 col-sm-9"><div class="checkbox"><label><input type="checkbox" ng-model="product.byWeight">На развес</label></div></div></div></form></tab><tab heading="Цена"><form role="form" class="form-horizontal" novalidate=""><div class="form-group"></div><div class="form-group"><label for="input-price" class="col-sm-3 control-label">Цена (Опт):</label><div class="col-sm-9"><input type="number" class="form-control" id="input-price" ng-model="product.discount.bulkPrice"></div></div><div class="form-group"><label for="input-price" class="col-sm-3 control-label">Цена (Розница):</label><div class="col-sm-9"><input type="number" class="form-control" id="input-bulkPrice" ng-model="product.price"></div></div><div class="form-group"><label for="input-discount" class="col-sm-3 control-label">Скидка:</label><div class="col-sm-9"><div class="input-group"><input type="number" class="form-control" id="input-discount" ng-model="product.discount.discount"><span class="input-group-addon">%</span></div></div></div><ul class="list-group"><li class="list-group-item"><span class="badge">{{product.category.discount}} %</span> Скидка в категории {{product.category.name}}</li></ul></form></tab></tabset></div><div class="modal-footer"><div class="btn-group btn-group-sm"><button type="button" class="btn btn-default" ng-click="$dismiss()">Отменить</button><button type="button" class="btn btn-primary" ng-click="save()">Сохранить</button></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/products/invoices/invoices.html",'<div class="row"><div class="col-sm-12"><div class="panel panel-default"><div class="panel-heading"><h4>Накладные</h4></div><div class="panel-body"><p ng-hide="invoices.length">Нет добавленных накладных</p><table class="table" ng-show="invoices.length"><thead><tr><th width="10%">Номер</th><th width="10%">Дата</th><th width="10%">Сумма</th><th width="10%">Контрагент</th><th width="50%">Товары</th><th width="5%"></th><th width="5%"></th></tr></thead><tbody><tr ng-repeat="invoice in invoices"><td>{{invoice.code}}</td><td>{{invoice.date}}</td><td>{{invoice.total}}</td><td>{{invoice.contractor}}</td><td><accordion><accordion-group heading="Показать ({{invoice.items.length}})"><table class="table"><thead><tr><th>Штрих-код</th><th>Наименование</th><th>Количество</th><th>Цена (приход)</th><th>Сумма</th></tr></thead><tbody><tr ng-repeat="item in invoice.items"><td>{{item.product.id}}</td><td>{{item.product.name}}</td><td>{{item.amount}}</td><td>{{item.price | number:2}}</td><td>{{item.price * item.amount | number:2}}</td></tr></tbody></table></accordion-group></accordion></td><td><a class="btn btn-default btn-sm disabled" ng-href="#/products/invoices/{{invoice.id}}"><span class="glyphicon glyphicon-edit"></span></a></td><td><button class="btn btn-danger btn-sm" ng-click="remove(invoice)"><span class="glyphicon glyphicon-trash"></span></button></td></tr></tbody></table></div><div class="panel-footer"><div class="btn-group btn-group-sm"><a type="button" class="btn btn-default" href="#/products/invoices/add"><span class="glyphicon glyphicon-file"></span> Новая накладная</a></div></div></div></div></div>')}])}(),function(t){try{t=angular.module("salesApp")}catch(n){t=angular.module("salesApp",[])}t.run(["$templateCache",function(t){t.put("app/products/invoices/add/add-invoice.html",'<div class="row"><div class="col-sm-12"><h3>Новая накладная</h3></div></div><div class="row"><alert ng-show="notFound" type="danger"><p>Товар с наименованием &laquo;{{newItem.product.name}}&raquo; не найден.</p><p><button type="button" class="btn btn-default btn-xs" ng-click="newProduct(newItem.product.id)">Добавить товар</button></p></alert></div><div class="row"><div class="col-sm-12"><div class="panel panel-default"><div class="panel-heading"><form class="form-inline" role="form"><div class="form-group"><label class="sr-only" for="invoice-number">Номер накладной</label><div class="input-group"><div class="input-group-addon">№</div><input type="text" class="form-control" id="invoice-number" placeholder="Номер" ng-model="invoice.code"></div></div><div class="form-group"><label class="sr-only" for="invoice-date">Дата</label><div class="input-group"><input type="text" class="form-control" id="invoice-date" datepicker-popup="dd.MM.yyyy" ng-model="invoice.date" is-open="opened" placeholder="Дата"><span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open($event)"><span class="glyphicon glyphicon-calendar"></span></button></span></div></div><div class="form-group"><label class="sr-only" for="invoice-contractor">Контрагент</label><div class="input-group"><select id="invoice-contractor" class="form-control"><option disabled="" selected="" value="">Контрагент</option></select><div class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></div></div></div></form></div><table class="table"><thead><tr><th>Наименование</th><th>Штрих-код</th><th>Количество</th><th>Цена, р</th><th>Сумма, р</th><th></th></tr></thead><tbody><tr><td><label class="sr-only" for="invoice-item-name">Наименование</label><input type="text" id="invoice-item-name" ng-model="newItem.product.name" list="invoice-item-name-list"><datalist id="invoice-item-name-list"><option ng-repeat="name in productNames" value="{{name}}"></option></datalist></td><td>{{newItem.product.id}}</td><td><label class="sr-only" for="invoice-item-amount">Количество</label><input type="number" id="invoice-item-amount" ng-model="newItem.amount"></td><td><label class="sr-only" for="invoice-item-incomePrice">Цена (приход)</label><input type="text" id="invoice-item-incomePrice" ng-model="newItem.incomePrice"></td><td>{{newItem.incomePrice * newItem.amount}}</td><td><button class="btn btn-default btn-sm" type="button" ng-class="{disabled: !newItem.product.id}" ng-click="addItem(invoice, newItem)"><span class="glyphicon glyphicon-plus"></span></button></td></tr><tr ng-repeat="item in invoice.items"><td>{{item.product.id}}</td><td>{{item.product.name}}</td><td>{{item.amount}}</td><td>{{item.price | number:2}}</td><td>{{item.product.price | number:2}}</td><td><button class="btn btn-danger btn-sm" type="button" ng-click="removeItem(invoice, item)"><span class="glyphicon glyphicon-trash"></span></button></td></tr></tbody></table><div class="panel-footer"><div class="btn-group btn-group-sm"><a class="btn btn-default" href="#/products/invoices">Отменить</a><input type="submit" class="btn btn-primary" ng-disabled="!invoice.code || !invoice.items.length" ng-click="save(invoice)" value="Сохранить"></div></div></div></div></div>')}])}();