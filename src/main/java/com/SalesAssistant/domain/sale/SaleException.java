package com.salesassistant.domain.sale;

/**
 * Created by mxostrov on 22.08.2014.
 */
public class SaleException extends Exception {

    public SaleException() {
    }

    public SaleException(String msg) {
        super(msg);
    }
}
