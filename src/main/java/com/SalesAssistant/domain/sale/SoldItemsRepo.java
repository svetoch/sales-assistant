package com.salesassistant.domain.sale;

import com.salesassistant.core.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by mxostrov on 22.08.2014.
 */
public interface SoldItemsRepo extends BaseRepository<SoldItem, Long> {


    @Query(value = "SELECT sl.items FROM  Sale sl WHERE sl.id = :param")
    List<SoldItem> findBySale(@Param("param") long id, Pageable pageable);


}
