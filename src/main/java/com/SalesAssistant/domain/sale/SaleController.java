package com.salesassistant.domain.sale;

import com.salesassistant.core.NotFoundException;
import com.salesassistant.domain.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mxostrov on 22.08.2014.
 */
@RestController
@RequestMapping(value = "/api/sales")
public class SaleController {

    @Autowired
    public SaleService saleService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Sale> findAll(@PageableDefault Pageable pageable,
                                  @RequestParam(value = "filter", required = false) String... filters) {
        return saleService.findWithFiltering(pageable, filters);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/items")
    public Iterable<SoldItem> fetchItems(@PageableDefault Pageable pageable, @PathVariable("id") long id) {
        return saleService.fetchItems(id, pageable);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/client")
    public Client findClient(@PathVariable("id") Sale sale) {
        return sale.getClient();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Sale findOne(@PathVariable("id") long id) throws NotFoundException {
        return saleService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Sale save(@RequestBody Sale sale) throws SaleException {
        return saleService.sale(sale);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") Sale sale) {
        saleService.delete(sale);
    }


}
