package com.salesassistant.domain.sale;

import com.salesassistant.domain.client.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/*
  * Entity stored what have been sold, by whom and when.
 */
@Entity
/*@JsonSerialize(using = SaleJsonSerializer.class)*/

@FilterDefs({@FilterDef(name = "client", parameters = @ParamDef(name = "val", type = "text"))
})

@Filters({@Filter(name = "client", condition = "client = :val"),
})

public class Sale {


    public Sale() {
    }

    @Id
    @GeneratedValue
    private long id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable
    private List<SoldItem> items = new ArrayList<>();

    @OneToOne
    private Client client;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Column
    private double total;

    @Column
    private Timestamp orderTime;


    public String getOrderTime() {
        return new SimpleDateFormat("dd.MM.YYYY HH:mm:ss").format(orderTime);
    }

    public void setOrderTime(Timestamp  orderTime) {
        this.orderTime = orderTime;
    }

    @PrePersist
    private void onCreate() {
        orderTime = new Timestamp(System.currentTimeMillis());
    }


    @JsonIgnore
    public List<SoldItem> getItems() {
        return items;
    }

    @JsonProperty
    public void setItems(List<SoldItem> items) {
        this.items = items;
    }


    public Client getClient() {
        return client;
    }


    public void setClient(Client client) {
        this.client = client;
    }


    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }


    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
