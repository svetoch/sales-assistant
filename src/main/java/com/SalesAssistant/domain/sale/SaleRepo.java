package com.salesassistant.domain.sale;

import com.salesassistant.core.BaseRepository;


public interface SaleRepo extends BaseRepository<Sale, Integer> {


}
