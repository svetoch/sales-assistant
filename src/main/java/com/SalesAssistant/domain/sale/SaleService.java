package com.salesassistant.domain.sale;

import com.salesassistant.core.BaseService;
import com.salesassistant.domain.client.Client;
import com.salesassistant.domain.client.ClientRepository;
import com.salesassistant.domain.storage.Product;
import com.salesassistant.domain.storage.ProductsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mxostrov on 22.08.2014.
 */
@Service
@Transactional
public class SaleService extends BaseService<Sale, Long> {

    @Autowired
    private SoldItemsRepo soldItemsRepo;

    @Autowired
    private ProductsRepo productsRepo;

    @Autowired
    private ClientRepository clientRepo;

    @Autowired
    public SaleService(SaleRepo repo) {
        super(repo);
    }


    public Iterable<SoldItem> fetchItems(long id, Pageable pageable) {
        PageImpl<SoldItem> page = new PageImpl<>(soldItemsRepo.findBySale(id, pageable));
        return page;
    }


    public Sale sale(Sale sale) throws SaleException {
        if (sale.getId() == 0) {
            Client client = clientRepo.findOne(sale.getClient().getId());
            if (client == null) {
                throw new SaleException("No such client");
            }

            sale.setClient(client);
            for (SoldItem soldItem : sale.getItems()) {
                Product product = productsRepo.findOne(soldItem.getOriginal().getId());
                int num = product.getAmount() - soldItem.getAmount();
                if (num < 0) {
                    throw new SaleException("Not enough products on storage");
                }
                product.setAmount(num);
            }
        }
        return repo.save(sale);
    }

    @Override
    public void delete(Sale sale) {
        for (SoldItem soldItem : sale.getItems()) {
            Product product = productsRepo.findOne(soldItem.getOriginal().getId());
            if (product != null) {
                product.setAmount(product.getAmount() - soldItem.getAmount());
                productsRepo.save(product);
            }
        }
        repo.delete(sale);
    }

}
