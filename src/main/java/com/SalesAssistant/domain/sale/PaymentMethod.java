package com.salesassistant.domain.sale;

/**
 * Created by mxostrov on 28.08.2014.
 */
public enum PaymentMethod {
    CARD, CASH
}
