package com.salesassistant.domain.sale;

import com.salesassistant.core.BaseProduct;
import com.salesassistant.domain.discount.DiscountType;
import com.salesassistant.domain.storage.Product;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

/**
 * Created by mxostrov on 23.07.2014.
 */

@Entity
public class SoldItem extends BaseProduct {


    @Id
    @GeneratedValue
    private long id;

    @Column
    @Enumerated(EnumType.STRING)
    private DiscountType discountType;

    @Column
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime orderTime;

    @Column
    private double discount;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @OneToOne
    private Product original;

    public Product getOriginal() {
        return original;
    }

    public void setOriginal(Product original) {
        this.original = original;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }


    public double getTotal() {
        return amount * price - price * 0.01 * discount;
    }

    public double getPriceWithDiscount() {
        return  price - price * 0.01 * discount;
    }

    @Column(nullable = true)
    @Override
    public String getName() {
        return name;
    }

    @PrePersist
    private void onCreate() {
        orderTime = new LocalDateTime();
    }


}
