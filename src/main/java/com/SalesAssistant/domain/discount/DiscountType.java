package com.salesassistant.domain.discount;

public enum DiscountType {
    NONE, CUSTOM, BIRTHDAY, PRODUCT, CATEGORY, CLIENT, BULK, GOOD_SALE, GOOD_CLIENT_ACTIVITY
}
