package com.salesassistant.domain.discount;

import com.salesassistant.core.BaseRepository;

public interface ProductDiscountRepo extends BaseRepository<ProductDiscount, Integer> {
}
