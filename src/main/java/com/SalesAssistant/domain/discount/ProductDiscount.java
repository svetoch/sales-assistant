package com.salesassistant.domain.discount;

import javax.persistence.*;

@Entity
public class ProductDiscount {

    @Id
    @GeneratedValue
    private int id;

    @Column(columnDefinition = "integer default -1")
    private int discount = 0;

    @Column(columnDefinition = "integer default -1")
    private double bulkPrice;

    @Column(columnDefinition = "integer default -1")
    private int bulkAmount;

    public ProductDiscount(int discount, double bulkPrice, int bulkAmount) {
        this.discount = discount;
        this.bulkPrice = bulkPrice;
        this.bulkAmount = bulkAmount;
    }

    public ProductDiscount() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public double getBulkPrice() {
        return bulkPrice;
    }

    public void setBulkPrice(double bulkPrice) {
        this.bulkPrice = bulkPrice;
    }

    public int getBulkAmount() {
        return bulkAmount;
    }

    public void setBulkAmount(int bulkAmount) {
        this.bulkAmount = bulkAmount;
    }
}
