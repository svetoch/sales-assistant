package com.salesassistant.domain.client;

import com.salesassistant.core.NotFoundException;
import com.salesassistant.domain.sale.SaleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mxostrov on 23.08.2014.
 */
@RestController
@RequestMapping(value = "/api/clients")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET)
    public Page<Client> findAll(@PageableDefault Pageable pageable,
                                @RequestParam(value = "filter", required = false) String... filters) {
        return clientService.findWithFiltering(pageable, filters);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Client findOne(@PathVariable("id") String id) throws NotFoundException {
        return clientService.findOne(id);
    }


    @RequestMapping(method = RequestMethod.POST)
    public Client save(@RequestBody Client client) throws SaleException {
        return clientService.save(client);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Client save(@PathVariable("id") String id, @RequestBody Client client) {
        client.setId(id);
        return clientService.save(client);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") String id) {
        clientService.delete(id);
    }


}
