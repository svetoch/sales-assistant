package com.salesassistant.domain.client;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
public class Client {

    private static final SimpleDateFormat dateFormat;

    static {
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern("dd.MM.yyyy");
    }

    public Client() {

    }

    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @Column
    private String email;

    @Column
    private Date dateOfBirth;

    @Column
    private int discount = 0;

    @Column
    private String telephone;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateFormat.format(dateOfBirth);
    }

    public void setDateOfBirth(String dateOfBirth) {
        try {
            this.dateOfBirth = dateFormat.parse(dateOfBirth);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
