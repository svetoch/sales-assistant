package com.salesassistant.domain.client;

import com.salesassistant.core.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mxostrov on 26.08.2014.
 */
@Service
public class ClientService extends BaseService<Client, String> {

    @Autowired
    public ClientService(ClientRepository repo) {
        super(repo);
    }
}
