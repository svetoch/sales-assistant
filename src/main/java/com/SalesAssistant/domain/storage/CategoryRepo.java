package com.salesassistant.domain.storage;

import com.salesassistant.core.BaseRepository;

/**
 * Created by mxostrov on 22.08.2014.
 */
public interface CategoryRepo extends BaseRepository<Category, Integer> {

}
