package com.salesassistant.domain.storage;

import com.salesassistant.core.BaseService;
import com.salesassistant.domain.discount.ProductDiscountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by mxostrov on 15.08.2014.
 */
@Service
@Transactional
public class StorageService extends BaseService<Product, String> {


    private ProductsRepo repo;

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private ProductDiscountRepo productDiscountRepo;

    @Autowired
    StorageService(ProductsRepo productsRepo) {
        super(productsRepo);
        this.repo = productsRepo;
    }

    @Override
    public void delete(String id) {
        Product product = repo.findOne(id);
        product.setHidden(true);
        repo.save(product);
    }

    public Iterable<Category> findAllCategories() {
        return categoryRepo.findAll();
    }


    public Category saveCategory(Category category) {
        return categoryRepo.save(category);
    }

    public Category findOneCategory(int id) {
        return categoryRepo.findOne(id);
    }
}
