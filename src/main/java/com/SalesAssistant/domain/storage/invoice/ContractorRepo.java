package com.salesassistant.domain.storage.invoice;

import com.salesassistant.core.BaseRepository;

/**
 * Created by mxostrov on 22.09.2014.
 */
public interface ContractorRepo  extends BaseRepository<Contractor, Integer> {

}
