package com.salesassistant.domain.storage.invoice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.*;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;


@FilterDefs({@FilterDef(name = "start", parameters = @ParamDef(name = "val", type = "text")),
        @FilterDef(name = "end", parameters = @ParamDef(name = "val", type = "text")),
        @FilterDef(name = "contractor", parameters = @ParamDef(name = "val", type = "text"))
})

@Filters({@Filter(name = "start", condition = "TRUNCATE(creation) >= :val"),
        @Filter(name = "end", condition = "TRUNCATE(creation) <= :val"),
        @Filter(name = "contractor", condition = "contractor = :val")
})
@Entity
public class Invoice {


    private final static String TIME_FORMAT = "dd.MM.YYYY";

    @Id
    private String id;


    @OneToOne
    private Contractor contractor;

/*    @Formula("(Select sum(it.amount*it.price) from Invoice_Item it join Invoice_Items its on its.invoice = id)")
    private int sum;*/


    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable
    private List<InvoiceItem> items = new ArrayList<>();

    @Column(name = "creation")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")

    private LocalDateTime date;

    public Invoice() {
    }

    @PrePersist
    private void onCreate() {
        date = new LocalDateTime();
    }

    @JsonIgnore
    public LocalDateTime getDateUnformatted() {
        return date;
    }


    public String getDate() {
        if(date != null) {
            return date.toString(TIME_FORMAT);
        }
        return null;

    }

    public void setDate(String date) {
        this.date = DateTimeFormat.forPattern(TIME_FORMAT).parseLocalDateTime(date);
    }

    public void setDateUnformated(LocalDateTime date) {
        this.date = date;
    }



    @JsonIgnore
    public List<InvoiceItem> getItems() {
        return items;
    }

    @JsonProperty
    public void setItems(List<InvoiceItem> items) {

        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

/*    public int getSum() {
        return sum;
    }*/

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof Invoice && ((Invoice) obj).getId() != null) {
            return id == ((Invoice) obj).getId();
        } else if(obj != null && obj instanceof Invoice){
            return super.equals(obj);
        }
        return false;
    }
}
