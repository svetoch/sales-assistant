package com.salesassistant.domain.storage.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mxostrov on 22.09.2014.
 */

@RequestMapping("/api/contractors")
@RestController
public class ContractorController {


    @Autowired
    public ContractorRepo contractorRepo;


    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Contractor> findAll(@PageableDefault Pageable pageable,
                                     @RequestParam(value = "filter", required = false) String... filters) {
        return contractorRepo.findWithFiltering(pageable, filters);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Contractor save(@RequestBody Contractor contractor) throws InvoiceException {
        return contractorRepo.save( contractor);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") int id) {
        contractorRepo.delete(id);
    }


}
