package com.salesassistant.domain.storage.invoice;

import com.salesassistant.core.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/api/invoices")
@RestController
public class InvoiceController {


    @Autowired
    public InvoiceService invoiceService;


    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Invoice> findAll(@PageableDefault Pageable pageable,
                                     @RequestParam(value = "filter", required = false) String... filters) {
        return invoiceService.findWithFiltering(pageable, filters);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Invoice findOne(@PathVariable("id") String id) throws NotFoundException {
        return invoiceService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/items")
    public Iterable<InvoiceItem> findDeliveredItems(@PageableDefault Pageable pageable, @PathVariable("id") String id) {
        return invoiceService.getItems(id, pageable);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Invoice add(@RequestBody Invoice invoice) throws InvoiceException {
        return invoiceService.add(invoice);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Invoice update(@RequestBody Invoice invoice) throws InvoiceException {
        return invoiceService.update(invoice);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody Invoice invoice) {
        invoiceService.delete(invoice);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") String id) {
        invoiceService.delete(id);
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InvoiceException.class)
    InvoiceException handleBadRequest(HttpServletRequest req, InvoiceException ex) {
        return ex;
    }


}
