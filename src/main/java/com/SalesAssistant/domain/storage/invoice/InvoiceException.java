package com.salesassistant.domain.storage.invoice;

public class InvoiceException extends Exception {

    public InvoiceException(String msg) {
        super(msg);
    }

    public InvoiceException() {

    }

    public InvoiceException(Exception ex) {
        super(ex);
    }

}
