package com.salesassistant.domain.storage.invoice;

import com.salesassistant.core.BaseRepository;

/**
 * Created by mxostrov on 10.08.2014.
 */

public interface InvoiceRepo extends BaseRepository<Invoice, String> {


}
