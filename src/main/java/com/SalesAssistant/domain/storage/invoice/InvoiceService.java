package com.salesassistant.domain.storage.invoice;

import com.salesassistant.core.BaseService;
import com.salesassistant.domain.storage.Product;
import com.salesassistant.domain.storage.ProductsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mxostrov on 20.08.2014.
 */
@Transactional
@Service
public class InvoiceService extends BaseService<Invoice, String> {

    private InvoiceRepo invoiceRepo;
    @Autowired
    private ProductsRepo productsRepo;

    @Autowired
    private InvoiceItemRepo invoiceItemRepo;

    @Autowired
    public InvoiceService(InvoiceRepo repo) {
        super(repo);
        this.invoiceRepo = repo;
    }


    public Iterable<InvoiceItem> getItems(String id, Pageable pageable) {
        PageImpl<InvoiceItem> page =
                new PageImpl<>(invoiceItemRepo.findByInvoice(id, pageable));
        return page;
    }

/*    public DeliveredItem addItem(Long id, DeliveredItem deliveredItem) {
*//*        deliveredItem.setDeliveryNote(deliveryNotesRepo.findOne(id));*//*
        return   deliveryItemRepo.save(deliveredItem);
    }*/

    public Invoice update(Invoice invoice) throws InvoiceException {
        if (invoice.getId() == null) {
            throw new InvoiceException("The invoice id must be specified!");
        } else {
            if (!repo.exists(invoice.getId())) {
                throw new InvoiceException("The invoice doesn't exists!");
            }
            Invoice invoiceOld = repo.getOne(invoice.getId());
            if(invoice.getDate() == null){
                invoice.setDate(repo.getOne(invoice.getId()).getDate());
            }
            for (InvoiceItem invoiceItemCurrent : invoice.getItems()) {
                InvoiceItem invoiceItemOld = invoiceItemRepo.findOne(invoiceItemCurrent.getId());
                Product product = productsRepo.findOne(invoiceItemCurrent.getProduct().getId());
                product.setIncomePrice(invoiceItemCurrent.getPrice());
                if (invoiceItemOld != null) {
                    product.setAmount(product.getAmount() - invoiceItemOld.getAmount() + invoiceItemCurrent.getAmount());
                } else { //when new item was added to invoice
                    product.setAmount(product.getAmount() + invoiceItemCurrent.getAmount());
                }
                if (product.getAmount() < 0) {
                    throw new InvoiceException("The product balance can not be less than zero");
                }

                productsRepo.save(product);
            }

            for(InvoiceItem invoiceItemOld : invoiceOld.getItems()) {
                if(!invoice.getItems().contains(invoiceItemOld)) {
                    Product product = invoiceItemOld.getProduct();
                    product.setAmount(product.getAmount() - invoiceItemOld.getAmount());
                    if (product.getAmount() < 0) {
                        throw new InvoiceException("The product balance can not be less than zero");
                    }
                }
            }

        }
        return repo.save(invoice);
    }



    public Invoice add(Invoice invoice) throws InvoiceException {
        if (invoice.getId() == null) {
            throw new InvoiceException("The invoice id must be specified!");
        } else {
            if (repo.exists(invoice.getId())) {
                throw new InvoiceException("The invoice already exists!");
            }
            for (InvoiceItem invoiceItem : invoice.getItems()) {
                Product product = productsRepo.findOne(invoiceItem.getProduct().getId());
                product.setAmount(product.getAmount() + invoiceItem.getAmount());
                product.setIncomePrice(invoiceItem.getPrice());
                productsRepo.save(product);
            }
        }
        return repo.save(invoice);
    }


}
