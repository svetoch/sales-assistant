package com.salesassistant.domain.storage.invoice;

import com.salesassistant.domain.storage.Product;

import javax.persistence.*;

/**
 * Created by mxostrov on 10.08.2014.
 */
@Entity
public class InvoiceItem {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    private Product product;


    @Column
    private double price;

    @Column
    private int amount;


    public InvoiceItem() {
    }

    public InvoiceItem(Product product, int amount, double price) {
        this.product = product;
        this.amount = amount;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


}
