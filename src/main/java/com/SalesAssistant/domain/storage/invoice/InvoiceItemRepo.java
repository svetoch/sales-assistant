package com.salesassistant.domain.storage.invoice;

import com.salesassistant.core.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by mxostrov on 20.08.2014.
 */
public interface InvoiceItemRepo extends BaseRepository<InvoiceItem, Long> {


    @Query(value = "SELECT i.items FROM  Invoice i WHERE i.id = :param")
    List<InvoiceItem> findByInvoice(@Param("param") String id, Pageable pageable);


}
