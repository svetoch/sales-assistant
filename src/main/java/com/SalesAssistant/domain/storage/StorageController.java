package com.salesassistant.domain.storage;

import com.salesassistant.core.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mxostrov on 15.08.2014.
 */
@RestController
@RequestMapping(value = "/api/products")
public class StorageController {

    @Autowired
    public StorageService storageService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Product> findAll(@PageableDefault Pageable pageable,
                                     @RequestParam(value = "filter", required = false) String... filters) {
        return storageService.findWithFiltering(pageable, filters);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Product findOne(@PathVariable("id") String id) throws NotFoundException {
        return storageService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/categories")
    public Iterable<Category> findAllCategories() {
        return storageService.findAllCategories();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/categories")
    public Category save(@RequestBody Category category) {
        return storageService.saveCategory(category);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/categories/{id}")
    public Category save(@PathVariable("id") int id, @RequestBody Category category) {
        category.setId(id);
        return storageService.saveCategory(category);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/categories/{id}")
    public Category findOneCategory(@PathVariable("id") int id) {
        return storageService.findOneCategory(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Product save(@RequestBody Product product) {
        return storageService.save(product);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Product save(@PathVariable("id") String id, @RequestBody Product product) {
        product.setId(id);
        return storageService.save(product);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") String id) {
        storageService.delete(id);
    }


}
