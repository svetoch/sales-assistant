package com.salesassistant.domain.storage;

import com.salesassistant.core.BaseProduct;
import com.salesassistant.core.Hidden;
import com.salesassistant.domain.discount.ProductDiscount;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
//storage
@FilterDefs({@FilterDef(name = "name", parameters = @ParamDef(name = "val", type = "text")),
        @FilterDef(name = "category", parameters = @ParamDef(name = "val", type = "text")),
        @FilterDef(name = "criticalAmount"),
        @FilterDef(name = "hidden")

})

@Filters({@Filter(name = "name", condition = "lower(name) like lower(CONCAT('%', :val, '%'))"),
        @Filter(name = "category", condition = "category = :val"),
        @Filter(name = "criticalAmount", condition = "((by_Weight = 'false' and amount<5) or (by_Weight = 'true' and amount<300))"),
        @Filter(name = "hidden", condition = "hidden = 'false'"),
})

@OnDelete(action = org.hibernate.annotations.OnDeleteAction.CASCADE)
public class Product extends BaseProduct implements Hidden, Serializable {

    public Product() {
    }


    @Id
    private String id;

    @Column
    private boolean byWeight;

    @ManyToOne
    private Category category;

    @OneToOne(cascade = CascadeType.ALL)
    private ProductDiscount productDiscount;

    @Column
    private boolean hidden;
/*
    @Formula(" (SELECT invoice_item.price " +
            " from invoice_item " +
            " where invoice_item.product = id) ")*/
    @Column
    private Double incomePrice;

    public Product(String id, String name, Category category, ProductDiscount discount, int num, double price, boolean byWeight) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.productDiscount = discount;
        this.amount = num;
        this.byWeight = byWeight;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductDiscount getDiscount() {
        return productDiscount;
    }

    public void setDiscount(ProductDiscount discount) {
        this.productDiscount = discount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isByWeight() {
        return byWeight;
    }

    public void setByWeight(boolean byWeight) {
        this.byWeight = byWeight;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public double getIncomePrice() {
        return incomePrice != null ? incomePrice : 0;
    }

    public void setIncomePrice(double incomePrice) {
        this.incomePrice = incomePrice;
    }
}
