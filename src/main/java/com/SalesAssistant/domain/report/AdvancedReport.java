package com.salesassistant.domain.report;

import org.hibernate.annotations.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable

@FilterDefs({@FilterDef(name = "start", parameters = @ParamDef(name = "val", type = "text")),
        @FilterDef(name = "end", parameters = @ParamDef(name = "val", type = "text"))
})

@Filters({@Filter(name = "start", condition = "TRUNCATE(order_date) >= :val"),
        @Filter(name = "end", condition = "TRUNCATE(order_date) <= :val"),

})
public class AdvancedReport {

    @Id
    private String id;

    @Column(name = "name")
    private String productName;

    private Long invoiceAmount;
    private Double invoicePrice;
    private Long soldAmount;
    private Double soldPrice;
    private String discountType;
    private Double discount;

    @Formula("sold_price*sold_amount")
    private Double soldSumPrice;

    private Long soldFrequency;

    @Formula("(sold_price-invoice_price)*sold_amount")
    private Double profit;

    @Formula("((sold_price-invoice_price)/sold_price)*100")
    private Double surchargePercent;

    public String getProductName() {
        return productName;
    }

    public long getInvoiceAmount() {
        return invoiceAmount == null ? 0 : invoiceAmount;
    }

    public double getInvoicePrice() {
        return invoicePrice == null ? 0 : invoicePrice;
    }

    public long getSoldAmount() {
        return soldAmount == null ? 0 : soldAmount;
    }

    public double getSoldPrice() {
        return soldPrice == null ? 0 : soldPrice;
    }

    public double getSoldSumPrice() {
        return soldSumPrice == null ? 0 : soldSumPrice;
    }

    public long getSoldFrequency() {
        return soldFrequency == null ? 0 : soldFrequency;
    }

    public double getProfit() {
        return profit == null ? 0 : profit;
    }

    public double getSurchargePercent() {
        return surchargePercent == null ? 0 : surchargePercent;
    }


    public String getDiscountType() {
        return discountType == null ? "none" : discountType;
    }

    public Double getDiscount() {
        return discount == null ? 0 : discount;
    }
}
