package com.salesassistant.domain.report;

import org.springframework.data.repository.Repository;

import java.util.Date;
import java.util.List;


/**
 * Created by mxostrov on 29.08.2014.
 */
public interface  DailyReportRepository  extends Repository<DailyProceed, String> {



    List<DailyProceed> findByOrderDate(Date date);

}
