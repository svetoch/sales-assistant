package com.salesassistant.domain.report;

import java.util.List;

/**
 * Created by mxostrov on 07.10.2014.
 */
public class DailyReport {

    private List<DailyProceed> dailyProceedList;
    private List<DailyReportItem> dailyReportItems;

    public DailyReport(List<DailyProceed> dailyProceedList, List<DailyReportItem> dailyReportItems) {
        this.dailyProceedList = dailyProceedList;
        this.dailyReportItems = dailyReportItems;
    }


    public List<DailyReportItem> getDailyReportItems() {
        return dailyReportItems;
    }

    public List<DailyProceed> getDailyProceedList() {
        return dailyProceedList;
    }


}
