package com.salesassistant.domain.report;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.sql.Date;


/**
 * Created by mxostrov on 29.08.2014.
 */

@Entity
@Immutable
@IdClass(ProceedPK.class)
public class DailyProceed {




    @Id
    private Date orderDate;

    private Double sum;

    @Id
    private String paymentMethod;


    public double getSum() {
        return sum;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public Date getOrderTime() {
        return orderDate;
    }


    private class ProceedPK {
    }


}
