package com.salesassistant.domain.report;

import org.springframework.data.repository.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by mxostrov on 29.08.2014.
 */
public interface DailyReportItemRepository extends Repository<DailyReportItem, String> {

   List<DailyReportItem> findByOrderDate(Date date);


}
