package com.salesassistant.domain.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping(value = "/api/report")
public class ReportController {

    @Autowired
    private ReportService reportService;



    @RequestMapping(method = RequestMethod.GET, value = "/daily/{day}")
    public DailyReport dailyReport(@PageableDefault Pageable pageable,
                                    @PathVariable("day") @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) Date day) {
        return reportService.dailyReport(day);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/advanced")
    public Iterable<AdvancedReport> advancedReport(@PageableDefault Pageable pageable,
                                                       @RequestParam(value = "filter", required = false) String... filters) {

        return reportService.advancedReport(pageable, filters);
    }


}
