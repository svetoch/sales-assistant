package com.salesassistant.domain.report;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by mxostrov on 08.10.2014.
 */
public  class ProceedPK implements Serializable {


        protected Date orderDate;
        protected String paymentMethod;

        public ProceedPK() {

        }

        public ProceedPK(Date orderDate, String paymentMethod) {
            this.orderDate = orderDate;
            this.paymentMethod = paymentMethod;
        }
        // TODO equals, hashCode

}
