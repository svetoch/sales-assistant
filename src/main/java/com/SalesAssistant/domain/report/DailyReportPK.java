package com.salesassistant.domain.report;

/**
 * Created by mxostrov on 08.10.2014.
 */

import java.io.Serializable;
import java.sql.Date;

public class DailyReportPK implements Serializable {
    protected Date orderDate;
    protected String name;

    public DailyReportPK() {}

    public DailyReportPK(Date orderDate, String name) {
        this.orderDate = orderDate;
        this.name = name;
    }
    // TODO equals, hashCode

}
