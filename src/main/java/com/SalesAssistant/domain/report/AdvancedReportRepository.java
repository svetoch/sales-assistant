package com.salesassistant.domain.report;

import com.salesassistant.core.BaseRepository;

/**
 * Created by mxostrov on 28.08.2014.
 */
public interface AdvancedReportRepository extends BaseRepository<AdvancedReport, String> {

}
