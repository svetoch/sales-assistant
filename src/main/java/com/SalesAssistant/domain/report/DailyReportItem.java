package com.salesassistant.domain.report;

/**
 * Created by mxostrov on 29.08.2014.
 */

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.sql.Date;

@Entity
@Immutable
@IdClass(DailyReportPK.class)
public class DailyReportItem {


    @Id
    private Date orderDate;
    @Id
    private String name;
    private Long num;
    private Double sum;
    private Long frequency;


    public String getName() {
        return name;
    }

    public Long getNum() {
        return num;
    }

    public Double getSum() {
        return sum;
    }

    public Long getFrequency() {
        return frequency;
    }
}
