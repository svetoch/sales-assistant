package com.salesassistant.domain.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * Created by mxostrov on 28.08.2014.
 */

@Service
public class ReportService {


  @Autowired
  private AdvancedReportRepository advancedReportRepository;

    @Autowired
    private DailyReportItemRepository dailyReportItemRepository;

    @Autowired
    private DailyReportRepository dailyReportRepository;





    public Iterable<AdvancedReport> advancedReport(Pageable pageable, String[] filters) {
        Iterable<AdvancedReport> advancedReports = advancedReportRepository.findWithFiltering(pageable, filters);
        return  advancedReports;
    }


    public DailyReport dailyReport(Date date) {
        List<DailyProceed> dailyProceeds = dailyReportRepository.findByOrderDate(date);
        List<DailyReportItem> dailyReportList = dailyReportItemRepository.findByOrderDate(date);
        return new DailyReport(dailyProceeds, dailyReportList);
    }
}
