package com.salesassistant.utils;

import com.salesassistant.domain.client.Client;
import com.salesassistant.domain.client.ClientRepository;
import com.salesassistant.domain.discount.DiscountType;
import com.salesassistant.domain.discount.ProductDiscount;
import com.salesassistant.domain.sale.PaymentMethod;
import com.salesassistant.domain.sale.Sale;
import com.salesassistant.domain.sale.SaleService;
import com.salesassistant.domain.sale.SoldItem;
import com.salesassistant.domain.storage.Category;
import com.salesassistant.domain.storage.Product;
import com.salesassistant.domain.storage.StorageService;
import com.salesassistant.domain.storage.invoice.Invoice;
import com.salesassistant.domain.storage.invoice.InvoiceException;
import com.salesassistant.domain.storage.invoice.InvoiceItem;
import com.salesassistant.domain.storage.invoice.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mxostrov on 20.08.2014.
 */
@Component
@Profile("dev")
public class DevUtils {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private StorageService storageService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SaleService saleService;


    List<Category> categoryList;

    private final static String PRODUCT_ID_PREFIX = "23456789abc";

    @PostConstruct
    public void init() {


        categoryList = new ArrayList<>();
        if(!storageService.findAllCategories().iterator().hasNext()) {
            categoryList.add(new Category("Чай"));
            categoryList.add(new Category("Кофе"));
            categoryList.add(new Category("Специи"));
            categoryList.add(new Category("Соль"));
        } else {
            return;
        }


        System.out.println("<--- INITIALIZING DOMAIN MODEL BEGIN... --->");
        Client client = new Client();
        client.setId("CLI01" + PRODUCT_ID_PREFIX);
        client.setName("Чехов Антон Павлович");
        client.setDateOfBirth("17.10.1989");
        client.setDiscount(7);
        clientRepository.save(client);

        Client client2 = new Client();
        client2.setId("CLI02" + PRODUCT_ID_PREFIX);
        client2.setName("Пупкин Василий Иванович");
        client2.setDateOfBirth("12.05.1992");
        client2.setDiscount(7);
        clientRepository.save(client2);



        for (Category category : categoryList) {
            storageService.saveCategory(category);
        }
        Product product = new Product("01" + PRODUCT_ID_PREFIX, "Чай синий ветер 50гр",
                null, null, 0, 130, false);
        InvoiceItem invoiceItem = new InvoiceItem(product, 100, 102);
        if(!invoiceService.exist("09999232")) {
            Invoice firstDelivery = new Invoice();
            firstDelivery.setId("09999232");

            storageService.save(product);

            firstDelivery.getItems().add(invoiceItem);
            firstDelivery.getItems().add(getDeliveryItems("02", "Чай молочный улун 50гр", false, firstDelivery));
            firstDelivery.getItems().add(getDeliveryItems("03", "Чай Китайский Болванчик", true, firstDelivery));
            firstDelivery.getItems().add(getDeliveryItems("04", "Чай Каркадэ", true, firstDelivery));
            try {
                invoiceService.add(firstDelivery);
            } catch (InvoiceException e) {
                e.printStackTrace();
            }
        }


        if(!invoiceService.exist("0999954645")) {
            Invoice secondDelivery = new Invoice();
            secondDelivery.setId("0999954645");
            secondDelivery.getItems().add(getDeliveryItems("05", "Кофе Джава", true, secondDelivery));
            secondDelivery.getItems().add(getDeliveryItems("06", "Кофе Бразилия Сул Де Минас", true, secondDelivery));
            secondDelivery.getItems().add(getDeliveryItems("07", "Соль красная 50гр", false, secondDelivery));
            secondDelivery.getItems().add(getDeliveryItems("08", "Тмин Индия", true, secondDelivery));
            invoiceItem = new InvoiceItem(product, 100, 102);
            secondDelivery.getItems().add(invoiceItem);
            try {
                invoiceService.add(secondDelivery);
            } catch (InvoiceException e) {
                e.printStackTrace();
            }

        }


        Sale sale = new Sale();
        sale.setClient(client);
        sale.setPaymentMethod(PaymentMethod.CASH);
        SoldItem soldItem = new SoldItem();
        soldItem.setAmount(40);
        soldItem.setPrice(product.getPrice());
        soldItem.setOriginal(product);
        soldItem.setDiscount(8.34);
        soldItem.setDiscountType(DiscountType.GOOD_SALE);
        sale.getItems().add(soldItem);
        sale.setTotal(soldItem.getTotal());
        saleService.save(sale);


        Sale sale2 = new Sale();
        sale2.setClient(client2);
        sale2.setPaymentMethod(PaymentMethod.CARD);
        SoldItem soldItem2 = new SoldItem();
        soldItem2.setAmount(5);
        soldItem2.setPrice(product.getPrice());
        soldItem2.setOriginal(product);
        soldItem2.setDiscount(7);
        soldItem2.setDiscountType(DiscountType.CLIENT);
        sale2.getItems().add(soldItem2);
        sale2.setTotal(soldItem2.getTotal());
        saleService.save(sale2);

        System.out.println("<--- INITIALIZING DOMAIN MODEL FINISH... --->");
    }


    private InvoiceItem getDeliveryItems(String id, String name, boolean byWeight, Invoice invoice) {
        Random random = new Random();
        Double price = Math.abs(random.nextDouble() * random.nextInt(500) + 10);
        Category category;
        if (name.startsWith("Чай")) {
            category = categoryList.get(0);
        } else if (name.startsWith("Кофе")) {
            category = categoryList.get(1);
        } else if (name.startsWith("Соль")) {
            category = categoryList.get(2);
        } else {
            category = categoryList.get(3);
        }

        ProductDiscount discount = new ProductDiscount(random.nextInt(10), price * 0.9, 10);
        Product product = new Product(id + PRODUCT_ID_PREFIX, name, category, discount, 0, price, byWeight);
        storageService.save(product);
        InvoiceItem invoiceItem = new InvoiceItem(product, Math.abs(random.nextInt(byWeight ? 800 : 30)), price * 0.8);
        return invoiceItem;
    }


}
