package com.salesassistant;

import com.salesassistant.gui.Controller;
import com.salesassistant.gui.MainJavaFXController;
import com.salesassistant.gui.SpringFxmlLoader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@ComponentScan
@EnableAutoConfiguration
@Configuration
public class MainEntryPoint extends Application {

    private static final String GUI_JAVA_FX = "-GUI_JAVA_FX";
    private static final String APPLICATION_NAME = "Sales Assistant";


    private volatile static ApplicationContext ctx;


    public static void main(String[] args) {
        ctx = SpringApplication.run(MainEntryPoint.class, args);
        launch();
/*        String url = "http://localhost:8888";
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Controller controller = (Controller)  new SpringFxmlLoader(ctx).load(MainJavaFXController.class);
        Scene scene = new Scene((Pane) controller.getView(), 800, 600);
        primaryStage.setTitle(APPLICATION_NAME);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
      System.exit(0);
    }
}
