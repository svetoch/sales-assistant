package com.salesassistant;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {

    @RequestMapping(value= {"/", "/clients/**", "/products/**", "/order/**"}, method = RequestMethod.GET)
    public String index(HttpServletRequest request, Model model) {
        String url = request.getRequestURI();
        if (url == null) {
            url = Constants.ROOT;
        }
        model.addAttribute(Constants.KEY_CLIENT_URL, url);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/exit")
    public String exit() {
      System.exit(0);
      return "exit";
    }

}

