package com.salesassistant.config;

import com.salesassistant.core.BaseRepositoryFactoryBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by mxostrov on 18.08.2014.
 */

@EnableJpaRepositories(
        repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class,
        basePackages = "com.salesassistant.domain")
@Configuration
public class JpaRepositoriesConfig {
}
