package com.salesassistant;

public class Constants {
    public static final String ROOT = "/";
    public static final String API_PREFIX = "/api";
    public static final String API_CLIENTS = API_PREFIX + "/clients";
    public static final String API_PRODUCTS = API_PREFIX + "/products";
    public static final String KEY_CLIENT_URL = "clientUrl";

    public static final String[] SERVER_PREFIXES = {API_PREFIX, "/styles", "/js", "/fonts"};

    private Constants() {

    }
}
