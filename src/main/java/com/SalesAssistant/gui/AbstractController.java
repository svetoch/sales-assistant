package com.salesassistant.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Created by Ostrovksie on 04.11.2014.
 */

public abstract class AbstractController implements Controller {

    private Pane view;

    public  Pane getView(){
        return view;
    }

    public void setView(Pane view){
        this.view= view;
    }

    public abstract void init() throws Exception;


}
