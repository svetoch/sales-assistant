package com.salesassistant.gui;

import javafx.scene.layout.Pane;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * Created by Ostrovksie on 04.11.2014.
 */
@Component
public class ClientJFXController extends AbstractController  {


    @Override
    public void init() throws Exception {

    }

    @Override
    public String getViewName() {
        return "clients.fxml";
    }
}
