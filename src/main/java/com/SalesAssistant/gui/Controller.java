package com.salesassistant.gui;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * Created by Ostrovksie on 08.11.2014.
 */
public interface Controller {

    Pane getView();
    void setView (Pane view);
    String getViewName();
}
