package com.salesassistant.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Created by mxostrov on 30.10.2014.
 */
public class SpringFxmlLoader
{

    private ApplicationContext context;

    public SpringFxmlLoader(ApplicationContext context)
    {
        this.context = context;
    }

    public Controller load(Class controllerClass) throws Exception {
             AbstractController instance = (AbstractController) context.getBean(controllerClass);
             FXMLLoader loader = new FXMLLoader();
             loader.setController(instance);
             Resource resource = context.getResource("classpath:" + instance.getViewName());
             loader.setLocation(resource.getURL());
             Pane view = (Pane) loader.load(resource.getInputStream());
             instance.setView(view);
             instance.init();
             return  instance;
    }



}