package com.salesassistant.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;


/**
 * Created by mxostrov on 29.10.2014.
 */

@Component
public class MainJavaFXController extends AbstractController {

    private static final String MAIN_VIEW_NAME = "main2.fxml";
    private Controller salesController;
    private  Controller clientController;
    private Controller currentWidget;
    @Autowired
    private ApplicationContext ctx;

    @FXML
    void showWidget(ActionEvent actionEvent) {
        String id = ((Button)actionEvent.getSource()).getId();
        Controller controller = null;
        switch (id) {
            case "salesBtn":
                controller = ( Controller ) ctx.getBean(SalesJFXController.class);
                break;
            case "clientsBtn":
                controller = (Controller ) ctx.getBean(ClientJFXController.class);
                break;
        }
        if(controller != null){
            getView().getChildren().remove(currentWidget.getView());
            currentWidget = controller;
            getView().getChildren().add( currentWidget.getView());
        }

    }



    @Override
    public void init() throws Exception {
        SpringFxmlLoader loader = new SpringFxmlLoader(ctx);
        salesController =  loader.load(SalesJFXController.class);
        currentWidget =  salesController;
        getView().getChildren().add(currentWidget.getView());
        clientController =  loader.load(ClientJFXController.class);

    }

    @Override
    public String getViewName() {
        return MAIN_VIEW_NAME;
    }
}
