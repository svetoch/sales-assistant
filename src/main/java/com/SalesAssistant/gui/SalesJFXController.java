package com.salesassistant.gui;

import javafx.scene.layout.Pane;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

/**
 * Created by Ostrovksie on 04.11.2014.
 */
@Controller
public class SalesJFXController extends AbstractController {



    @Override
    public String getViewName() {
        return "sales3.fxml";
    }


    @Override
    public void init() throws Exception {

    }
}
