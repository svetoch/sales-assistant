package com.salesassistant.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * Created by mxostrov on 20.08.2014.
 */
public interface BaseRepository<T, ID extends Serializable>
        extends JpaRepository<T, ID> {

    public Page<T> findWithFiltering(Pageable pageable, String[] filters);

}
