package com.salesassistant.core;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by mxostrov on 18.08.2014.
 */

@NoRepositoryBean
public class BaseRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements BaseRepository<T, ID> {

    private EntityManager entityManager;


    public BaseRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public Page<T> findWithFiltering(Pageable pageable, String[] filters) {
        Session session = entityManager.unwrap(Session.class);
/*        session.enableFilter("hidden");*/
        if (filters != null) {
            for (String filterPair : filters) {
                String[] filterDef = filterPair.split(":");
                Filter filter = session.enableFilter(filterDef[0]);
                if (filterDef.length > 1) {
                    filter.setParameter("val", filterPair.split(":")[1]);
                }
            }
        }
        return findAll(pageable);
    }

}

