package com.salesassistant.core;

/**
 * Created by mxostrov on 27.08.2014.
 */
public interface Hidden {

    boolean isHidden();

}
