package com.salesassistant.core;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public class BaseProduct {


    public BaseProduct() {
    }


    protected String name;

    @Column(nullable = false)
    protected double price;


    @Column(nullable = false)
    protected int amount;

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


}
