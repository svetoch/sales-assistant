package com.salesassistant.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;

/**
 * Created by mxostrov on 15.08.2014.
 */
public class BaseService<S, ID extends Serializable> {

    protected BaseRepository<S, ID> repo;


    public BaseService(BaseRepository repo) {
        this.repo = repo;
    }

    public S findOne(ID id) throws NotFoundException {
        S s = repo.findOne(id);
        if (s == null || (s instanceof Hidden && ((Hidden) s).isHidden())) {
            throw new NotFoundException();
        }
        return repo.findOne(id);
    }

    public Iterable<S> findAll() {
        return repo.findAll();
    }

    public S save(S s) {
        return repo.save(s);
    }

    public void delete(S s) {
        repo.delete(s);
    }

    public void delete(ID id) {
        repo.delete(id);
    }

    public Page<S> findWithFiltering(Pageable pageable, String[] filters) {
        return repo.findWithFiltering(pageable, filters);
    }

    public boolean exist(ID id){
        return repo.exists(id);
    }

}
