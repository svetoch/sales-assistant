# README #

SalesAssistant is a CRM web application with extra marketing capabilities.

## Installation
The application requires a working Java EE servlet container such as Apache Tomcat. It's then deployed to it as usual.

## Development

SDE requirements:

* Java 1.7
* Apache Maven 3.2.1 or higher
* Any servlet container with support of Servlet 3.0 or higher

Please contact this repo's administrator on any questions. Pull requests are currently not permitted.